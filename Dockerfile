# This is the base dockerfile that we're working from
# Don't try to do it from scratch unless you want to write an OS
FROM node:12-alpine

COPY requirements.txt /requirements.txt

# RUN lets you execute commands inside your docker image
# It is executed when building and goes inside the image as a layer
# Used for installing a package or directory
RUN apk add python3-dev &&\
    apk add py3-pip &&\
    apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add bash && \
    apk add vim && \
    apk add postgresql-dev
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN npm config set python "/usr/lib/python3"


ENV PATH="/usr/lib/python3:${PATH}"
ENV PYTHON="/usr/lib/python3"

# Set the working directory for a command
RUN mkdir -p /plant_planet
WORKDIR /plant_planet
COPY plant_src /plant_planet
#COPY package.json /plant_planet/package.json
WORKDIR /plant_planet/plant-planet/src
RUN npm install -g react-scripts
RUN npm install
RUN npm install typescript
RUN npm install --save react-router-dom
RUN npm install --save-dev jest

EXPOSE $PORT
ENV NUXT_HOST==0.0.0.0
ENV PROXY_API=$PROXY_API
ENV PROXY_LOGIN=$PROXY_LOGIN
EXPOSE 3000
EXPOSE 8080

# Copy a file from you directory to a directory inside the image
#COPY start.sh /start.sh

# CMD defines a default command to run when the container starts
RUN ls
WORKDIR /
COPY start.sh /start.sh
CMD ["/start.sh"]
