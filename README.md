# Plant Planet - A Website for Native Protection

---

| Name | EID | GitLab ID |
|---------------------|---------|-----------|
| Nina Ahmed          | na24538 | ninaahmed |
| Erica Martinez      | em42282 | Junimos   |
| Varshinee Sreekanth | vs23368 | varshinee1 |
| Elizabeth Rodriguez | ear3475 | elizabeth.amanda.rodriguez |
| Lauren Jernigan     | lsj445  | ljernigan447 |

---
## Phase 4
<strong>Git SHA: </strong> 16d81b30bbdd83a2c1579afd30782b2819d1f59b

<strong>Project Leader: </strong> Varshinee Sreekanth

<strong>GitLab Pipeline: </strong> [Pipeline](https://gitlab.com/elizabeth.amanda.rodriguez/swe-f20-project/-/pipelines/220817276)

<strong>Link to website: </strong> [The Plant Planet](https://theplantpla.net)

<strong>Estimated Completion Time: </strong> 20 hours each member

<strong>Actual Completion Time: </strong> 20 hours each member

---
## Phase 3
<strong>Git SHA: </strong> 1d0d2e615453fb2ef3e9a2a70f070d0d97cfb8ed

<strong>Project Leader: </strong> Elizabeth Rodriguez

<strong>GitLab Pipeline: </strong> [Pipeline](https://gitlab.com/elizabeth.amanda.rodriguez/swe-f20-project/-/pipelines/212536514)

<strong>Link to website: </strong> [The Plant Planet](https://theplantpla.net)

<strong>Estimated Completion Time: </strong> 30 hours each member

<strong>Actual Completion Time: </strong> 30 hours each member

---
## Phase 2
<strong>Git SHA: </strong> 38e4ada48bd7f83d81179ae1bb31692dbb6a8ef2

<strong>Project Leader: </strong> Lauren Jernigan

<strong>GitLab Pipeline: </strong> [Pipeline](https://gitlab.com/elizabeth.amanda.rodriguez/swe-f20-project/-/pipelines/205486015)

<strong>Link to website: </strong> [The Plant Planet](https://theplantpla.net)

<strong>Estimated Completion Time: </strong> 35 hours each member

<strong>Actual Completion Time: </strong> 45 hours each member

---
## Phase 1
<strong>Git SHA: </strong>42d167a96e19c0c39fd30b0e1df8ea6b69be73f8

<strong>Project Leader: </strong> Nina Ahmed

<strong>GitLab Pipeline: </strong> Not needed until Phase II

<strong>Link to Website: </strong> [The Plant Planet](https://theplantpla.net)

<strong>Estimated Completion Time: </strong> 20 hours each member

<strong>Actual Completion Time: </strong> 25 hours each member

---
<strong>Comments</strong>

This is a website to encourage people to grow native plants in their gardens to support local wildlife and ecosystems that depend on native flora.

We have three models:
* Animals
* Plants
* Geographic Locations
