import React, { useState } from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import algoliasearch from "algoliasearch/lite";
import {
    InstantSearch,
    Hits,
    Index,
    SearchBox,
    Configure,
    Highlight,
    Stats,
    Pagination
} from "react-instantsearch-dom";
import "instantsearch.css/themes/reset.css";

const useStyles = makeStyles({
    root: {
        minWidth: 50,
        maxWidth: 300
    },
    bullet: {
        display: "inline-block",
        margin: "0 2px",
        transform: "scale(0.8)"
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    },
    media: {
        height: "100px",
        width: "auto",
        align: "center"
    }
});

// props: show plants, animals, and/or locations in the results
export default function SearchModel(props: any) {
    const classes = useStyles();
    const searchClient = algoliasearch(
        "EVKDYAKCQD",
        "d2a1e20a93a2ab5c1ed8ac79f211d777"
    );

    // card for a plant result
    const PlantHit = ({ hit }: any) => (
        <div>
            <Typography variant="h5" component="h2">
                <Highlight
                    attribute="common_name"
                    hit={hit}
                    tagName="mark"
                />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Location:
                <Highlight
                    attribute="country_code"
                    hit={hit}
                    tagName="mark"
                />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Family:
                <Highlight attribute="family" hit={hit} tagName="mark" />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Genus:
                <Highlight attribute="genus" hit={hit} tagName="mark" />
            </Typography>
            <img
                className={classes.media}
                src={hit.img_url[0]}
                alt="search"
            />
            <Button href={`/plant/${hit.plant_id}`} size="small">
                Learn More
            </Button>
        </div>
    );

    // card for an animal result
    const AnimalHit = ({ hit }: any) => (
        <div>
            <Typography variant="h5" component="h2">
                <Highlight
                    attribute="common_name"
                    hit={hit}
                    tagName="mark"
                />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Location:
                <Highlight
                    attribute="country_code"
                    hit={hit}
                    tagName="mark"
                />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Family:
                <Highlight attribute="family" hit={hit} tagName="mark" />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Genus:
                <Highlight attribute="genus" hit={hit} tagName="mark" />
            </Typography>
            <img
                className={classes.media}
                src={hit.img_url[0]}
                alt="search"
            />
            <Button href={`/animal/${hit.animal_id}`} size="small">
                Learn More
            </Button>
        </div>
    );

    // card for a location result
    const LocationHit = ({ hit }: any) => (
        <div>
            <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
            >
                Capital City:
                <Highlight
                    attribute="capital_city"
                    hit={hit}
                    tagName="mark"
                />
            </Typography>
            <Typography variant="h5" component="h2">
                <Highlight attribute="state" hit={hit} tagName="mark" />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Country:
                <Highlight attribute="country" hit={hit} tagName="mark" />
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
                Population: {hit.population}
            </Typography>
            <img
                className={classes.media}
                src={hit.state_flag_url}
                alt="search"
            />
            <Button href={`/location/${hit.location_id}`} size="small">
                Learn More
            </Button>
        </div>
    );

    // booleans that represent what instances the user wants to search
    const [showPlant, setShowPlant] = useState(Boolean);
    const [showAnimal, setShowAnimal] = useState(Boolean);
    const [showLocation, setShowLocation] = useState(Boolean);

    // initial search term, if forwarded from a model page / searching navbar
    const [refinement, setRefinement] = useState(String);

    // set display booleans and initial search term
    useState(() => {
        setShowPlant(
            props.location.state == null || props.location.state.showPlant
        );
        setShowAnimal(
            props.location.state == null || props.location.state.showAnimal
        );
        setShowLocation(
            props.location.state == null ||
                props.location.state.showLocation
        );
        if (
            props.location.state != null &&
            props.location.state.refinement != null
        ) {
            setRefinement(props.location.state.refinement);
        } else {
            setRefinement("");
        }
    });

    // switch between showing instances for the models and hiding them
    function toggleShowPlant() {
        setShowPlant(!showPlant);
    }

    function toggleShowAnimal() {
        setShowAnimal(!showAnimal);
    }

    function toggleShowLocation() {
        setShowLocation(!showLocation);
    }

    // conditional rendering for the instances of each model
    let plantIndex;
    if (showPlant) {
        plantIndex = (
            <Index indexName="plants">
                <Configure hitsPerPage={4} />
                <Typography variant="h4" align="center">
                    Plant Model
                </Typography>
                <Hits hitComponent={PlantHit} />
                <Pagination />
            </Index>
        );
    }

    let animalIndex;
    if (showAnimal) {
        if (showAnimal) {
            animalIndex = (
                <Index indexName="animals">
                    <Configure hitsPerPage={4} />
                    <Typography variant="h4" align="center">
                        Animal Model
                    </Typography>
                    <Hits hitComponent={AnimalHit} />
                    <Pagination />
                </Index>
            );
        }
    }

    let locationIndex;
    if (showLocation) {
        locationIndex = (
            <Index indexName="locations">
                <Configure hitsPerPage={4} />
                <Typography variant="h4" align="center">
                    Location Model
                </Typography>
                <Hits hitComponent={LocationHit} />
                <Pagination />
            </Index>
        );
    }

    return (
        <>
            <div className="ais-InstantSearch">
                <InstantSearch
                    indexName="plants"
                    searchClient={searchClient}
                >
                    <SearchBox defaultRefinement={refinement} />
                    <Stats />

                    {/* Roggle displaying instances of each model */}
                    <Button onClick={toggleShowPlant}>
                        Show/Hide Plants
                    </Button>
                    <Button onClick={toggleShowAnimal}>
                        Show/Hide Animals
                    </Button>
                    <Button onClick={toggleShowLocation}>
                        Show/Hide Locations
                    </Button>

                    <p></p>
                    <p></p>

                    {plantIndex}

                    <p></p>
                    <p></p>

                    {animalIndex}

                    <p></p>
                    <p></p>

                    {locationIndex}

                    <p></p>
                    <p></p>
                </InstantSearch>
            </div>
        </>
    );
}
