import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import InstanceTable from "../InstanceTable";
import SearchBar from "material-ui-search-bar";
import FilterBar from "../Filter";

// family vals to filter by
const names = [
    "Family A-E",
    "Family F-J",
    "Family K-O",
    "Family P-T",
    "Family U-Z"
];

// genus vals to filter by
const genusVals = [
    "Genus A-E",
    "Genus F-J",
    "Genus K-O",
    "Genus P-T",
    "Genus U-Z"
];

export default function PlantModel(props: any) {
    const [animalData, setAnimalData] = useState(Object);

    // holds subsets with certain filters applied
    const [filteredFamilies, setFilteredFamilies] = useState<Object[]>([]);
    const [filteredGenuses, setFilteredGenuses] = useState<Object[]>([]);
    const [filteredLocs, setFilteredLocs] = useState<Object[]>([]);

    // holds the vals we're currently filtering by
    const [familyFilterVal, setFamilyFilterVal] = React.useState<string[]>(
        []
    );
    const [genusFilterVal, setGenusFilterVal] = React.useState<string[]>(
        []
    );
    const [locFilterVal, setLocFilterVal] = React.useState<string[]>([]);
    const [animalsFull, setAnimalsFull] = useState<Object[]>([]);

    const [searchTerm, setSearchTerm] = useState(String);

    // makes API call to get info for all plants
    useEffect(() => {
        fetch("/api/getplants")
            .then(res => res.json())
            .then(data => {
                setAnimalData(data);

                let allAnimals: Object[] = Object.keys(data).map(key => {
                    let animal = data[key];
                    return {
                        image:
                            animal.img_url != null
                                ? animal.img_url[0]
                                : null,
                        commonName: animal.common_name,
                        family: animal.family,
                        genus: animal.genus,
                        location: animal.country_code[0],
                        id: animal.plant_id
                    };
                });

                setAnimalsFull(animalsFull => allAnimals);
                setFilteredFamilies(filteredFamilies => allAnimals);
                setFilteredGenuses(filteredGenuses => allAnimals);
                setFilteredLocs(filteredLocs => allAnimals);
            });
    }, []);

    // default output if loading
    if (animalData === null) {
        return <h1>Could not load animal data</h1>;
    }

    // handles filerval change for family
    const handleChange = (event: React.ChangeEvent<{ value: any }>) => {
        let newAnimals: Object[] = [];
        if (event.target.value.length === 0) {
            newAnimals = newAnimals.concat(animalsFull);
        }
        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            let endBound = filterVal.substring(
                filterVal.length - 1,
                filterVal.length
            );
            let startBound = filterVal.substring(
                filterVal.length - 3,
                filterVal.length - 2
            );

            newAnimals = newAnimals.concat(
                animalsFull.filter((animal: any) => {
                    let startChar = animal.family
                        .substring(0, 1)
                        .toUpperCase();
                    return (
                        startChar >= startBound && startChar <= endBound
                    );
                })
            );
        }
        newAnimals = newAnimals.filter((c, index) => {
            return newAnimals.indexOf(c) === index;
        });
        setFilteredFamilies(filteredFamilies => newAnimals);
        setFamilyFilterVal(event.target.value as string[]);
    };

    // handles filter val change for genus
    const handleGenusChange = (
        event: React.ChangeEvent<{ value: any }>
    ) => {
        let newAnimals: Object[] = [];
        if (event.target.value.length === 0) {
            newAnimals = newAnimals.concat(animalsFull);
        }

        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            let endBound = filterVal.substring(
                filterVal.length - 1,
                filterVal.length
            );
            let startBound = filterVal.substring(
                filterVal.length - 3,
                filterVal.length - 2
            );

            newAnimals = newAnimals.concat(
                animalsFull.filter((animal: any) => {
                    let startChar = animal.genus
                        .substring(0, 1)
                        .toUpperCase();
                    return (
                        startChar >= startBound && startChar <= endBound
                    );
                })
            );
        }

        newAnimals = newAnimals.filter((c, index) => {
            return newAnimals.indexOf(c) === index;
        });
        setFilteredGenuses(filteredGenuses => newAnimals);
        setGenusFilterVal(event.target.value as string[]);
    };

    // callback, filters by location
    const handleLocChange = (event: React.ChangeEvent<{ value: any }>) => {
        let newAnimals: Object[] = [];
        if (event.target.value.length === 0) {
            newAnimals = newAnimals.concat(animalsFull);
        }

        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            newAnimals = newAnimals.concat(
                animalsFull.filter((animal: any) => {
                    return animal.location === filterVal;
                })
            );
        }

        newAnimals = newAnimals.filter((c, index) => {
            return newAnimals.indexOf(c) === index;
        });
        setFilteredLocs(filteredLocs => newAnimals);
        setLocFilterVal(event.target.value as string[]);
    };

    // callback for search, redirects to search page
    function handleSearch() {
        props.history.push({
            pathname: "/search",
            state: {
                showPlant: true,
                refinement: searchTerm
            }
        });
    }

    // used to set filter drop downs
    const filterInfo = [
        {
            name: "Filter by Family",
            value: familyFilterVal,
            change: handleChange,
            vals: names
        },
        {
            name: "Filter by Genus",
            value: genusFilterVal,
            change: handleGenusChange,
            vals: genusVals
        },
        {
            name: "Filter by Location",
            value: locFilterVal,
            change: handleLocChange,
            vals: ["CA", "US"]
        }
    ];

    return (
        <>
            <br />
            <Typography variant="h3" align="center" color="inherit">
                Our Plants
            </Typography>
            <br />
            <Typography
                variant="body1"
                align="center"
                color="inherit"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                This is the page with all our plant data! Here you will
                find info about native plants, their needs, and the animals
                that depend on them!
            </Typography>
            <Typography
                variant="body1"
                align="center"
                color="inherit"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                Click on the table column names to sort that column in
                ascending or descending order! Also,{" "}
                <b>you can deselect filter values</b> by clicking on them a
                second time in the drop down menu.
            </Typography>
            <br />

            <div style={{ paddingLeft: "2em", paddingRight: "2em" }}>
                <SearchBar
                    onChange={newValue => setSearchTerm(newValue)}
                    onRequestSearch={handleSearch}
                />
            </div>

            {/* Filter options */}
            <div style={{ flex: 1 }}>
                {filterInfo.map(val => {
                    return <FilterBar vals={val} />;
                })}
            </div>

            {/* Displays table of models */}
            <div style={{ paddingLeft: "6em", paddingRight: "6em" }}>
                <InstanceTable
                    instances={filteredFamilies.filter(
                        value =>
                            filteredGenuses.includes(value) &&
                            filteredLocs.includes(value)
                    )}
                    type="plant"
                />
            </div>
        </>
    );
}
