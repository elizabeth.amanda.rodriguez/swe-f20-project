import React from "react";
import { Typography } from "@material-ui/core";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            paddingBottom: "2em"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.primary
        }
    })
);

// displays the basic taxonomy and other info for the plant instances
export default function PlantTemplate(props: any) {
    const classes = useStyles();
    const notAvail = "N/A";

    if (props.plantData == null) {
        return <h1>Could not load basic info</h1>;
    }

    return (
        <>
            {/* Grid with basic info */}
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">Family</Typography>
                    <Typography variant="subtitle2">
                        {props.plantData.family || notAvail}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">Genus</Typography>
                    <Typography variant="subtitle2">
                        {props.plantData.genus || notAvail}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">Toxicity</Typography>
                    <Typography variant="subtitle2">
                        {props.plantData.toxicity || notAvail}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">
                        Primary Location
                    </Typography>
                    <Typography variant="subtitle2">
                        {props.plantData.country_code[0] || notAvail}
                    </Typography>
                </Paper>
            </Grid>
        </>
    );
}
