import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import Slideshow from "../../SitewideComponents/Slideshow";
import InstanceMap from "./../InstanceMap";
import defaultImage from "./../../../img/leaves-background.jpg";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PlantTemplateGrid from "./PlantTemplateGrid";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            paddingBottom: "2em"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.primary
        }
    })
);

export default function PlantTemplate(props: any) {
    const classes = useStyles();
    const [plantData, setPlantData] = useState(Object);
    const [locations, setLocations] = useState(Array<Object>());
    const [animal_ids, setAnimals] = useState(Array<any>());

    // makes API call to get all info
    useEffect(() => {
        fetch("/api/getplant/" + props.match.params.id)
            .then(res => res.json())
            .then(data => {
                setPlantData(data);
                if (data.location_ids != null) {
                    for (let i = 0; i < data.location_ids.length; i++) {
                        let curr_id = data.location_ids[i];

                        fetch("/api/getlocation/" + data.location_ids[i])
                            .then(res => res.json())
                            .then(locdata => {
                                setLocations(location_ids =>
                                    location_ids.concat({
                                        id: curr_id,
                                        name: locdata.state
                                    })
                                );

                                if (locdata.animal_ids != null) {
                                    for (
                                        let i = 0;
                                        i < locdata.animal_ids.length &&
                                        animal_ids.length < 20;
                                        i++
                                    ) {
                                        let animal_id =
                                            locdata.animal_ids[i];
                                        // gets all relevant animals from the native location
                                        fetch(
                                            "/api/getanimal/" +
                                                locdata.animal_ids[i]
                                        )
                                            .then(res => res.json())
                                            .then(pdata => {
                                                setAnimals(animal_ids =>
                                                    animal_ids.concat({
                                                        id: animal_id,
                                                        commonName:
                                                            pdata.common_name
                                                    })
                                                );
                                            });
                                    }
                                }
                            });
                    }
                }
            });
    }, []);

    // default loading output
    if (plantData.plant_id == null) {
        return (
            <>
                <br />
                <CircularProgress />
            </>
        );
    }

    // sets images, if present
    var images =
        plantData.img_url != null
            ? plantData.img_url.map((val: any) => {
                  return { image: val };
              })
            : [{ image: defaultImage }];

    return (
        <>
            <div style={{ paddingLeft: "3em", paddingRight: "3em" }}>
                <br />
                <Typography variant="h3" align="center" color="inherit">
                    {plantData.common_name}
                </Typography>
                <Typography variant="h6" align="center" color="inherit">
                    <i>{plantData.scientific_name}</i>
                </Typography>
                <div style={{ width: "70em", margin: "auto" }}>
                    <Slideshow images={images} />
                </div>
                <br />

                <Typography variant="h5" align="left" color="inherit">
                    Basic Info
                </Typography>
                <br />
                <div className={classes.root}>
                    <Grid container spacing={3}>
                        <PlantTemplateGrid plantData={plantData} />

                        {/* Displays relevant models and links to them */}
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Relevant Animals
                                </Typography>
                                {animal_ids
                                    .filter(
                                        (v, i, a) =>
                                            a.findIndex(
                                                t => t.id === v.id
                                            ) === i
                                    )
                                    .map((animal: any) => {
                                        return (
                                            <>
                                                <Link
                                                    to={`/animal/${animal.id}`}
                                                >
                                                    {animal.commonName}
                                                </Link>{" "}
                                                <br />
                                            </>
                                        );
                                    })}
                            </Paper>
                        </Grid>
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Native Locations
                                </Typography>
                                {locations.map((location: any) => {
                                    return (
                                        <>
                                            <Link
                                                to={`/location/${location.id}`}
                                            >
                                                {location.name}
                                            </Link>{" "}
                                            <br />
                                        </>
                                    );
                                })}
                            </Paper>
                        </Grid>
                    </Grid>
                </div>

                <Typography variant="h5" align="left" color="inherit">
                    Native Areas (Drag to move, scroll to zoom)
                </Typography>
                <br />
                <InstanceMap
                    lat={plantData.latitude}
                    lng={plantData.longitude}
                />
            </div>
        </>
    );
}
