import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import InstanceTable from "../InstanceTable";
import SearchBar from "material-ui-search-bar";
import FilterBar from "../Filter";

// values for family filter values
const names = [
    "Family A-E",
    "Family F-J",
    "Family K-O",
    "Family P-T",
    "Family U-Z"
];

// genus values to filter by
const genusVals = [
    "Genus A-E",
    "Genus F-J",
    "Genus K-O",
    "Genus P-T",
    "Genus U-Z"
];

export default function AnimalModel(props: any) {
    const [animalData, setAnimalData] = useState(Object);

    // filtered sets of animal data
    const [filteredFamilies, setFilteredFamilies] = useState<Object[]>([]);
    const [filteredGenuses, setFilteredGenuses] = useState<Object[]>([]);
    const [filteredLocs, setFilteredLocs] = useState<Object[]>([]);
    const [animalsFull, setAnimalsFull] = useState<Object[]>([]);

    // state vars for which filter values are selected
    const [familyFilterVal, setFamilyFilterVal] = React.useState<string[]>(
        []
    );
    const [genusFilterVal, setGenusFilterVal] = React.useState<string[]>(
        []
    );
    const [locFilterVal, setLocFilterVal] = React.useState<string[]>([]);

    const [searchTerm, setSearchTerm] = useState(String);

    // makes call to API and initializes values
    useEffect(() => {
        fetch("/api/getanimals")
            .then(res => res.json())
            .then(data => {
                setAnimalData(data);

                let allAnimals: Object[] = Object.keys(data).map(key => {
                    let animal = data[key];
                    return {
                        image:
                            animal.img_url != null
                                ? animal.img_url[0]
                                : null,
                        commonName: animal.common_name,
                        family: animal.family,
                        genus: animal.genus,
                        location: animal.country_code[0],
                        id: animal.animal_id
                    };
                });

                setAnimalsFull(animalsFull => allAnimals);
                setFilteredFamilies(filteredFamilies => allAnimals);
                setFilteredGenuses(filteredGenuses => allAnimals);
                setFilteredLocs(filteredLocs => allAnimals);
            });
    }, []);

    // shows the loading page before the API call gets back
    if (animalData === null) {
        return <h1>Could not load animal data</h1>;
    }

    // handles a change in the "filter by family" bar
    const handleChange = (event: React.ChangeEvent<{ value: any }>) => {
        let newAnimals: Object[] = [];
        if (event.target.value.length === 0) {
            newAnimals = newAnimals.concat(animalsFull);
        }
        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            let endBound = filterVal.substring(
                filterVal.length - 1,
                filterVal.length
            );
            let startBound = filterVal.substring(
                filterVal.length - 3,
                filterVal.length - 2
            );
            // filters out non-relevant data
            newAnimals = newAnimals.concat(
                animalsFull.filter((animal: any) => {
                    let startChar = animal.family
                        .substring(0, 1)
                        .toUpperCase();
                    return (
                        startChar >= startBound && startChar <= endBound
                    );
                })
            );
        }
        newAnimals = newAnimals.filter((c, index) => {
            return newAnimals.indexOf(c) === index;
        });
        setFilteredFamilies(filteredFamilies => newAnimals);
        setFamilyFilterVal(event.target.value as string[]);
    };

    // handles a change in the "filter by genus" bar
    const handleGenusChange = (
        event: React.ChangeEvent<{ value: any }>
    ) => {
        let newAnimals: Object[] = [];
        if (event.target.value.length === 0) {
            newAnimals = newAnimals.concat(animalsFull);
        }

        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            let endBound = filterVal.substring(
                filterVal.length - 1,
                filterVal.length
            );
            let startBound = filterVal.substring(
                filterVal.length - 3,
                filterVal.length - 2
            );

            newAnimals = newAnimals.concat(
                animalsFull.filter((animal: any) => {
                    let startChar = animal.genus
                        .substring(0, 1)
                        .toUpperCase();
                    return (
                        startChar >= startBound && startChar <= endBound
                    );
                })
            );
        }

        newAnimals = newAnimals.filter((c, index) => {
            return newAnimals.indexOf(c) === index;
        });
        setFilteredGenuses(filteredGenuses => newAnimals);
        setGenusFilterVal(event.target.value as string[]);
    };

    // handles a change in the "filter by location" bar
    const handleLocChange = (event: React.ChangeEvent<{ value: any }>) => {
        let newAnimals: Object[] = [];
        if (event.target.value.length === 0) {
            newAnimals = newAnimals.concat(animalsFull);
        }

        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            newAnimals = newAnimals.concat(
                animalsFull.filter((animal: any) => {
                    return animal.location === filterVal;
                })
            );
        }

        newAnimals = newAnimals.filter((c, index) => {
            return newAnimals.indexOf(c) === index;
        });
        setFilteredLocs(filteredLocs => newAnimals);
        setLocFilterVal(event.target.value as string[]);
    };

    // handles redirecting to search page
    function handleSearch() {
        props.history.push({
            pathname: "/search",
            state: {
                showAnimal: true,
                refinement: searchTerm
            }
        });
    }

    // used to set filter drop downs
    const filterInfo = [
        {
            name: "Filter by Family",
            value: familyFilterVal,
            change: handleChange,
            vals: names
        },
        {
            name: "Filter by Genus",
            value: genusFilterVal,
            change: handleGenusChange,
            vals: genusVals
        },
        {
            name: "Filter by Location",
            value: locFilterVal,
            change: handleLocChange,
            vals: ["CA", "US"]
        }
    ];

    return (
        <>
            <br />
            <Typography variant="h3" align="center" color="inherit">
                Our Animals
            </Typography>
            <br />
            <Typography
                variant="body1"
                align="center"
                color="inherit"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                This is the page with all of our animal data! Here you will
                find specific details about animals, including their diets
                and typical dimensions!
            </Typography>
            <Typography
                variant="body1"
                align="center"
                color="inherit"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                Click on the table column names to sort that column in
                ascending or descending order! Also,{" "}
                <b>you can deselect filter values</b> by clicking on them a
                second time in the drop down menu.
            </Typography>
            <br />
            {/* Search bar */}
            <div style={{ paddingLeft: "2em", paddingRight: "2em" }}>
                <SearchBar
                    onChange={newValue => setSearchTerm(newValue)}
                    onRequestSearch={handleSearch}
                />
            </div>

            {/* Filter options */}
            <div style={{ flex: 1 }}>
                {filterInfo.map(val => {
                    return <FilterBar vals={val} />;
                })}
            </div>

            {/* Displays the model table itself */}
            <div style={{ paddingLeft: "10em", paddingRight: "10em" }}>
                <InstanceTable
                    instances={filteredFamilies.filter(
                        value =>
                            filteredGenuses.includes(value) &&
                            filteredLocs.includes(value)
                    )}
                    type="animal"
                />
            </div>
        </>
    );
}
