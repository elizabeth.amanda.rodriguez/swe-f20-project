import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import Slideshow from "../../SitewideComponents/Slideshow";
import InstanceMap from "../InstanceMap";
import defaultImage from "./../../../img/default_animal.jpg";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";

// CSS styles
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            paddingBottom: "2em"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.primary
        }
    })
);

export default function AnimalTemplate(props: any) {
    const classes = useStyles();
    const [animalData, setAnimalData] = useState(Object);
    // connects to other models
    const [locations, setLocations] = useState(Array<Object>());
    const [plant_ids, setPlants] = useState(Array<any>());

    // makes API call to back end
    useEffect(() => {
        fetch("/api/getanimal/" + props.match.params.id)
            .then(res => res.json())
            .then(data => {
                setAnimalData(() => data);

                if (data.location_ids != null) {
                    for (let i = 0; i < data.location_ids.length; i++) {
                        let curr_id = data.location_ids[i];

                        fetch("/api/getlocation/" + data.location_ids[i])
                            .then(res => res.json())
                            .then(locdata => {
                                setLocations(location_ids =>
                                    location_ids.concat({
                                        id: curr_id,
                                        name: locdata.state
                                    })
                                );
                                // gathers relevant plant data from location endpoint
                                if (locdata.plant_ids != null) {
                                    for (
                                        let i = 0;
                                        i < locdata.plant_ids.length &&
                                        i < 20;
                                        i++
                                    ) {
                                        let plant_id =
                                            locdata.plant_ids[i];

                                        fetch(
                                            "/api/getplant/" +
                                                locdata.plant_ids[i]
                                        )
                                            .then(res => res.json())
                                            .then(pdata => {
                                                setPlants(plant_ids =>
                                                    plant_ids.concat({
                                                        id: plant_id,
                                                        commonName:
                                                            pdata.common_name
                                                    })
                                                );
                                            });
                                    }
                                }
                            });
                    }
                }
            });
    }, []);

    // loading render
    if (animalData.animal_id == null) {
        return (
            <>
                <br />
                <CircularProgress />
            </>
        );
    }

    // sets images, if present
    var images =
        animalData.img_url != null
            ? animalData.img_url.map((val: any) => {
                  return { image: val };
              })
            : [{ image: defaultImage }];

    const basicInfo = [
        {
            title: "Kingdom",
            value: animalData.kingdom
        },
        {
            title: "Phylum",
            value: animalData.phylum
        },
        {
            title: "Class",
            value: animalData.class
        },
        {
            title: "Order",
            value: animalData.animal_order
        },
        {
            title: "Family",
            value: animalData.family
        },
        {
            title: "Genus",
            value: animalData.genus
        }
    ];

    return (
        <>
            <div style={{ paddingLeft: "3em", paddingRight: "3em" }}>
                <br />
                <Typography variant="h3" align="center" color="inherit">
                    {animalData.common_name}
                </Typography>
                <Typography variant="h6" align="center" color="inherit">
                    <i>{animalData.scientific_name}</i>
                </Typography>
                <div style={{ width: "70em", margin: "auto" }}>
                    <Slideshow images={images} />
                </div>
                <br />

                <Typography variant="h5" align="left" color="inherit">
                    Basic Info
                </Typography>
                <br />
                <div className={classes.root}>
                    {/* Displays basic info */}
                    <Grid container spacing={3}>
                        {basicInfo.map(val => {
                            return (
                                <Grid item xs={4}>
                                    <Paper className={classes.paper}>
                                        <Typography variant="subtitle1">
                                            {val.title}
                                        </Typography>
                                        <Typography variant="subtitle2">
                                            {val.value}
                                        </Typography>
                                    </Paper>
                                </Grid>
                            );
                        })}
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Informal Taxonomy
                                </Typography>
                                <Typography variant="body1">
                                    {animalData.taxonomy}
                                </Typography>
                            </Paper>
                        </Grid>
                        {/* Shows relevant plants/animals */}
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Relevant Plants
                                </Typography>
                                {plant_ids
                                    .filter(
                                        (v, i, a) =>
                                            a.findIndex(
                                                t => t.id === v.id
                                            ) === i
                                    )
                                    .map((plant: any) => {
                                        return (
                                            <>
                                                <Link
                                                    to={`/plant/${plant.id}`}
                                                >
                                                    {plant.commonName}
                                                </Link>{" "}
                                                <br />
                                            </>
                                        );
                                    })}
                            </Paper>
                        </Grid>
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Native Locations
                                </Typography>
                                {locations.map((location: any) => {
                                    return (
                                        <>
                                            <Link
                                                to={`/location/${location.id}`}
                                            >
                                                {location.name}
                                            </Link>{" "}
                                            <br />
                                        </>
                                    );
                                })}
                            </Paper>
                        </Grid>
                    </Grid>
                </div>

                <Typography variant="h5" align="left" color="inherit">
                    Native Areas (Drag to move, scroll to zoom)
                </Typography>
                <br />
                {/* Shows map of native area */}
                <InstanceMap
                    lat={animalData.latitude}
                    lng={animalData.longitude}
                />
            </div>
        </>
    );
}
