import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import GridLoc from "./Grid";
import { Pagination } from "@material-ui/lab";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import SearchBar from "material-ui-search-bar";
import FilterBar from "../../Filter";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            "& > *": {
                margin: theme.spacing(1)
            }
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 200,
            maxWidth: 300
        }
    })
);

// values we can sort by
const names = [
    "Country Name Ascending",
    "Country Name Descending",
    "State Name Ascending",
    "State Name Descending",
    "Population Ascending",
    "Population Descending",
    "Capital City Ascending",
    "Capital City Descending"
];

// population values we can filter by
const populationFilterVals = [
    "Population 100,000 - 1,000,000",
    "Population 1,000,001 - 3,000,000",
    "Population 3,000,001 - 6,000,000",
    "Population 6,000,001 - 10,000,000",
    "Population 10,000,001 - 40,000,000"
];

// props: page number, comes in through URL at /location/page/:id
export default function LocationModel(props: any) {
    const classes = useStyles();

    let numPerPage = 9;
    let totalInstances = 60;
    let [currPage, setCurrPage] = useState(0);
    const [sortVal, setSortVal] = useState(String);

    // filtered sets, based on each filter val
    const [filteredCountry, setFilteredCountry] = useState<Object[]>([]);
    const [filteredLocs, setFilteredLocs] = useState<Object[]>([]);
    const [filteredPops, setFilteredPops] = useState<Object[]>([]);
    const [sortedLocs, setSortedLocs] = useState<Object[]>([]);

    const [countryFilterVal, setCountryFilterVal] = React.useState<
        string[]
    >([]);
    const [popFilterVal, setPopFilterVal] = React.useState<string[]>([]);

    const [searchTerm, setSearchTerm] = useState(String);

    // makes call to back end API
    useEffect(() => {
        fetch("/api/getlocations")
            .then(res => res.json())
            .then(data => {
                let everyLocation: Object[] = Object.keys(data).map(
                    key => {
                        let location = data[key];
                        return {
                            state: location.state,
                            country: location.country,
                            population: location.population,
                            state_flag_url: location.state_flag_url,
                            capital_city: location.capital_city,
                            location_id: location.location_id
                        };
                    }
                );
                setFilteredLocs(filteredLocs => everyLocation);
                setFilteredPops(filteredPops => everyLocation);
                setFilteredCountry(filteredCountry => everyLocation);
                setSortedLocs(sortedLocs => everyLocation);
            });
    }, []);

    // loading page
    if (sortedLocs.length === 0) {
        return (
            <>
                <br />
                <CircularProgress />
            </>
        );
    }

    // gets instances for one page
    let locations: Object[] = filteredLocs.slice(
        currPage * numPerPage,
        currPage * numPerPage + numPerPage
    );

    // changes the page
    function pageChangeHandle(pageNum: number) {
        pageNum = pageNum - 1;
        locations = filteredLocs.slice(
            pageNum * numPerPage,
            pageNum * numPerPage + pageNum
        );
        setCurrPage(pageNum);
    }

    // comparator for sorting
    function sortingHelper(field: String, ascending: boolean) {
        return filteredLocs.sort((a: any, b: any) => {
            let one = a[field + ""];
            let two = b[field + ""];
            if (ascending) return one > two ? 1 : two > one ? -1 : 0;
            else return one < two ? 1 : two < one ? -1 : 0;
        });
    }

    // sorts locations and re-renders
    const handleSortChange = (
        event: React.ChangeEvent<{ value: any }>
    ) => {
        let filterVal = event.target.value;
        let newLocations: Object[] = [];

        let terms = filterVal.split(" ");
        let field = terms[0].toLowerCase();
        if (field === "capital") {
            field = "capital_city";
        }
        let ascending = terms[terms.length - 1] === "Ascending";

        newLocations = newLocations.concat(
            sortingHelper(field, ascending)
        );
        setSortedLocs(sortedLocs => newLocations);
        setFilteredLocs(filteredLocs => newLocations);
        setSortVal(filterVal);
    };

    // filters values by vountry
    const handleCountryFilterChange = (
        event: React.ChangeEvent<{ value: any }>
    ) => {
        let newLocs: Object[] = [];
        if (event.target.value.length === 0) {
            newLocs = newLocs.concat(sortedLocs);
        }

        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            newLocs = newLocs.concat(
                sortedLocs.filter((loc: any) => {
                    return loc.country === filterVal;
                })
            );
        }

        newLocs = newLocs.filter((c: Object, index: number) => {
            return newLocs.indexOf(c) === index;
        });
        // set filteredLocs
        setFilteredLocs(filteredLocs =>
            newLocs.filter(val => {
                return filteredPops.includes(val);
            })
        );
        setFilteredCountry(filteredCountry => newLocs);
        setCountryFilterVal(event.target.value as string[]);
    };

    // filters values by population
    const handlePopFilterChange = (
        event: React.ChangeEvent<{ value: any }>
    ) => {
        let newLocs: Object[] = [];
        if (event.target.value.length === 0) {
            newLocs = newLocs.concat(sortedLocs);
        }

        for (let i = 0; i < event.target.value.length; i++) {
            let filterVal = event.target.value[i];

            let terms = filterVal.split(" ");
            let popLow = parseInt(
                terms[terms.length - 3].replace(/,/g, "")
            );
            let popHigh = parseInt(
                terms[terms.length - 1].replace(/,/g, "")
            );

            newLocs = newLocs.concat(
                sortedLocs.filter((loc: any) => {
                    return (
                        loc.population >= popLow &&
                        loc.population <= popHigh
                    );
                })
            );
        }

        newLocs = newLocs.filter((c: Object, index: number) => {
            return newLocs.indexOf(c) === index;
        });
        // set filteredLocs
        setFilteredLocs(filteredLocs =>
            newLocs.filter(val => {
                return filteredCountry.includes(val);
            })
        );
        setFilteredPops(filteredPops => newLocs);
        setPopFilterVal(event.target.value as string[]);
    };

    // callback for searching
    function handleSearch() {
        props.history.push({
            pathname: "/search",
            state: {
                showLocation: true,
                refinement: searchTerm
            }
        });
    }

    // used to set filter drop downs
    const filterInfo = [
        {
            name: "Filter By Country",
            value: countryFilterVal,
            change: handleCountryFilterChange,
            vals: ["United States", "Canada"]
        },
        {
            name: "Filter By Population",
            value: popFilterVal,
            change: handlePopFilterChange,
            vals: populationFilterVals
        }
    ];

    return (
        <>
            <br />
            <Typography variant="h3" align="center">
                Our Locations
            </Typography>
            <br />
            <Typography
                variant="body1"
                align="center"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                This is the page with all our location data! Here you will
                find info about locations, general statistics, and the
                plants and animals native to them!
            </Typography>
            <Typography
                variant="body1"
                align="center"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                Click the dropdowns below to sort or filter.
            </Typography>
            <Typography
                variant="body1"
                align="center"
                style={{ paddingLeft: "8em", paddingRight: "8em" }}
            >
                Total number of instances: {totalInstances}
            </Typography>
            <br />
            <div style={{ paddingLeft: "2em", paddingRight: "2em" }}>
                <SearchBar
                    onChange={newValue => setSearchTerm(newValue)}
                    onRequestSearch={handleSearch}
                />
            </div>

            {/* Sorting */}
            <div style={{ flex: 1 }}>
                <FormControl className={classes.formControl}>
                    <InputLabel>Sort By:</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={sortVal}
                        onChange={handleSortChange}
                    >
                        {names.map(name => (
                            <MenuItem value={name}>{name + "\n"}</MenuItem>
                        ))}
                    </Select>
                </FormControl>

                {/* Filter Vals */}
                {filterInfo.map(val => {
                    return <FilterBar vals={val} />;
                })}
            </div>

            <br />
            <GridLoc locs={locations}></GridLoc>
            <br />
            <div className={classes.button}>
                <Pagination
                    count={Math.ceil(filteredLocs.length / numPerPage)}
                    onChange={(event: object, page: number) =>
                        pageChangeHandle(page)
                    }
                    color="secondary"
                />
            </div>
            <br />
        </>
    );
}
