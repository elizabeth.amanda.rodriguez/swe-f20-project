import React from "react";
import LocationCard from "./LocationCard";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
    gridContainer: {
        paddingLeft: "40px",
        paddingRight: "40px"
    }
});

/*
  Used to make the location model page
  props: an array called locs with objects of the form:
    {
    name: 
    country: 
    population:
    flag: url
    continent:
    id:  
  }
*/
const GridLoc = (props: any) => {
    const classes = useStyles();
    return (
        <Grid
            container
            spacing={3}
            className={classes.gridContainer}
            justify="center"
        >
            {props.locs &&
                props.locs.map((loc: any) => {
                    return (
                        <Grid item xs={12} sm={6} md={3}>
                            <LocationCard location={loc} />
                        </Grid>
                    );
                })}
        </Grid>
    );
};
export default GridLoc;
