import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    root: {
        minWidth: 50
    },
    bullet: {
        display: "inline-block",
        margin: "0 2px",
        transform: "scale(0.8)"
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    },
    media: {
        height: 80,
        width: "60%",
        align: "center",
        marginLeft: "20%"
    }
});

// used to show one location instance on the model page
// props: an object names location with format:
/*
  {
    name: 
    country: 
    population:
    flag: url
    capital:
    id:  
  }
*/
export default function OutlinedCard(props: any) {
    const classes = useStyles();

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography
                    className={classes.title}
                    color="textSecondary"
                    gutterBottom
                >
                    Capital City: {props.location.capital_city}
                </Typography>
                <Typography variant="h5" component="h2">
                    {props.location.state}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    Country: {props.location.country}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    Population:{" "}
                    {props.location.population.toLocaleString()}
                </Typography>
                <CardMedia
                    className={classes.media}
                    image={props.location.state_flag_url}
                ></CardMedia>
            </CardContent>
            <CardActions>
                <Button
                    href={`/location/${props.location.location_id}`}
                    size="small"
                >
                    Learn More
                </Button>
            </CardActions>
        </Card>
    );
}
