import React from "react";
import { Typography } from "@material-ui/core";
import MLink from "@material-ui/core/Link";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            paddingBottom: "2em"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.primary
        }
    })
);

// creates the grid to display basic data for the loction template
export default function LocationTemplate(props: any) {
    const classes = useStyles();

    if (props.locData == null) {
        return <h1>Could not load basic info</h1>;
    }
    return (
        <>
            {/* Grid with basic info */}
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">
                        Capital City
                    </Typography>
                    <Typography variant="subtitle2">
                        {props.locData.capital_city}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">Population</Typography>
                    <Typography variant="body1">
                        {props.locData.population.toLocaleString()}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">Website</Typography>
                    <Typography variant="subtitle2">
                        {(props.locData.website != " " && (
                            <MLink href={props.locData.website}>
                                Click here!
                            </MLink>
                        )) ||
                            "N/A"}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}>
                    <Typography variant="body1">
                        Latitude: {props.locData.latitude}
                    </Typography>
                    <Typography variant="body1">
                        Longitude: {props.locData.longitude}
                    </Typography>
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Typography variant="subtitle1">
                        Twitter URL
                    </Typography>
                    <Typography variant="subtitle2">
                        {(props.locData.twitter_url != " " && (
                            <MLink href={props.locData.twitter_url}>
                                {props.locData.twitter_url}
                            </MLink>
                        )) ||
                            "N/A"}
                    </Typography>
                </Paper>
            </Grid>
        </>
    );
}
