import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import Slideshow from "../../../SitewideComponents/Slideshow";
import InstanceMap from "../../InstanceMap";
import defaultImage from "../../../../img/default-location.jpg";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import LocTemplateGrid from "./LocTemplateGrid";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            paddingBottom: "2em"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.primary
        }
    })
);

export default function LocationTemplate(props: any) {
    // holds info about relevant models
    const classes = useStyles();
    const [locData, setLocData] = useState(Object);
    const [plant_ids, setPlants] = useState(Array<Object>());
    const [animal_ids, setAnimals] = useState(Array<Object>());

    // makes api call
    useEffect(() => {
        fetch("/api/getlocation/" + props.match.params.id)
            .then(res => res.json())
            .then(data => {
                setLocData(data);
                if (data.plant_ids != null) {
                    for (let i = 0; i < data.plant_ids.length; i++) {
                        let curr_id = data.plant_ids[i];
                        // gets every plant name from its id
                        fetch("/api/getplant/" + data.plant_ids[i])
                            .then(res => res.json())
                            .then(data => {
                                setPlants(plant_ids =>
                                    plant_ids.concat({
                                        id: curr_id,
                                        commonName: data.common_name
                                    })
                                );
                            });
                    }
                }
                if (data.animal_ids != null) {
                    for (let i = 0; i < data.animal_ids.length; i++) {
                        let curr_id = data.animal_ids[i];
                        // gets every animal name from its id
                        fetch("/api/getanimal/" + data.animal_ids[i])
                            .then(res => res.json())
                            .then(data => {
                                setAnimals(animal_ids =>
                                    animal_ids.concat({
                                        id: curr_id,
                                        commonName: data.common_name
                                    })
                                );
                            });
                    }
                }
            });
    }, []);

    // loading page
    if (locData.location_id == null) {
        return (
            <>
                <br />
                <CircularProgress />
            </>
        );
    }

    // sets images, if present
    var images: any[] = [];

    if (locData.landscape_background_url != " ") {
        images.push({ image: locData.landscape_background_url });
    }
    if (locData.state_flag_url != " ") {
        images.push({ image: locData.state_flag_url });
    }
    if (locData.map_image_url != " ") {
        images.push({ image: locData.map_image_url });
    }
    if (images.length == 0) {
        images.push({ image: defaultImage });
    }

    return (
        <>
            <div style={{ paddingLeft: "3em", paddingRight: "3em" }}>
                <br />
                <Typography variant="h3" align="center" color="inherit">
                    {locData.state}
                </Typography>
                <Typography variant="h6" align="center" color="inherit">
                    <i>{locData.country}</i>
                </Typography>
                <div style={{ width: "70em", margin: "auto" }}>
                    <Slideshow images={images} />
                </div>
                <br />

                <Typography variant="h5" align="left" color="inherit">
                    Basic Info
                </Typography>
                <br />
                <div className={classes.root}>
                    <Grid container spacing={3}>
                        {/* Basic Info in grid format */}
                        <LocTemplateGrid locData={locData} />
                        {/* Links to related models */}
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Native Plants
                                </Typography>
                                {plant_ids.map((plant: any) => {
                                    return (
                                        <>
                                            <Link
                                                to={`/plant/${plant.id}`}
                                            >
                                                {plant.commonName}
                                            </Link>{" "}
                                            <br />
                                        </>
                                    );
                                })}
                            </Paper>
                        </Grid>
                        <Grid item xs={6}>
                            <Paper className={classes.paper}>
                                <Typography variant="subtitle1">
                                    Native Animals
                                </Typography>
                                {animal_ids.map((animal: any) => {
                                    return (
                                        <>
                                            <Link
                                                to={`/animal/${animal.id}`}
                                            >
                                                {animal.commonName}
                                            </Link>{" "}
                                            <br />
                                        </>
                                    );
                                })}
                            </Paper>
                        </Grid>
                    </Grid>
                </div>
                {/* Map of location */}
                <Typography variant="h5" align="left" color="inherit">
                    Native Areas (Drag to move, scroll to zoom)
                </Typography>
                <br />
                <InstanceMap
                    lat={locData.latitude}
                    lng={locData.longitude}
                />
            </div>
        </>
    );
}
