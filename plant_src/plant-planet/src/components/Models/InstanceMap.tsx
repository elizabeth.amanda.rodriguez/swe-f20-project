import React from "react";

import mapboxgl from "mapbox-gl";
import "../../css/styles.css";

mapboxgl.accessToken =
    "pk.eyJ1IjoidmFyc2h1c3JlZSIsImEiOiJja2djb2oxbTMwc3VlMnJxdXh6OTV4dzZoIn0.eKytBlrguj87-Q5EpH10LA";

// creates a map using Mapbox for instance pages
export default class InstanceMap extends React.Component<
    { lng: any; lat: any },
    { lng: any; lat: any; zoom: any }
> {
    private mapContainer: HTMLElement | null | undefined = undefined;
    //private map: Map | undefined;
    constructor(props: any) {
        super(props);
        this.state = {
            // coordinates for Texas, default
            lng: typeof props.lng == "number" ? props.lng : -99.25061,
            lat: typeof props.lat == "number" ? props.lat : 31.25044,
            zoom: 5
        };
    }

    componentDidMount() {
        const map = new mapboxgl.Map({
            container:
                this.mapContainer === undefined || this.mapContainer === null
                    ? ""
                    : this.mapContainer,
            style: "mapbox://styles/mapbox/streets-v11",
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom
        });

        map.on("move", () => {
            this.setState({
                lng: map.getCenter().lng.toFixed(4),
                lat: map.getCenter().lat.toFixed(4),
                zoom: map.getZoom().toFixed(2)
            });
        });
        this.forceUpdate();
    }

    render() {
        return (
            <div>
                <div
                    ref={el => (this.mapContainer = el)}
                    className="mapContainer"
                />
            </div>
        );
    }
}
