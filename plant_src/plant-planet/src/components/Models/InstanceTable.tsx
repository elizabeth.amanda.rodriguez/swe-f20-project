import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { Link } from "react-router-dom";
import default_img from "./../../img/default_model_image.jpg";

// defines each column in table
interface Column {
    id: "image" | "commonName" | "family" | "genus" | "location";
    label: string;
    minWidth?: number;
    align?: "right";
    format?: (value: number) => string;
    disablePadding: boolean;
    numeric: boolean;
}

// defines propoerties of each column
const columns: Column[] = [
    { id: "image", label: "Image", disablePadding: false, numeric: false },
    {
        id: "commonName",
        label: "Common Name",
        minWidth: 50,
        disablePadding: false,
        numeric: false
    },
    {
        id: "family",
        label: "Family",
        minWidth: 50,
        disablePadding: false,
        numeric: false
    },
    {
        id: "genus",
        label: "Genus",
        minWidth: 50,
        disablePadding: false,
        numeric: false
    },
    {
        id: "location",
        label: "Location",
        minWidth: 50,
        disablePadding: false,
        numeric: false
    }
];

// data types for our columns
interface Data {
    image: string;
    commonName: string;
    family: String;
    genus: String;
    location: String;
    id: String;
}

// creates a Data object
function createData(
    image: string,
    commonName: string,
    family: String,
    genus: String,
    location: String,
    id: String
): Data {
    return { image, commonName, family, genus, location, id };
}

// used for sorting
function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

type Order = "asc" | "desc";

// for sorting
function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string }
) => number {
    return order === "desc"
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

// for sorting
function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map(
        (el, index) => [el, index] as [T, number]
    );
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: "100%"
        },
        paper: {
            width: "100%",
            marginBottom: theme.spacing(2)
        },
        table: {
            minWidth: 750
        },
        visuallyHidden: {
            border: 0,
            clip: "rect(0 0 0 0)",
            height: 1,
            margin: -1,
            overflow: "hidden",
            padding: 0,
            position: "absolute",
            top: 20,
            width: 1
        }
    })
);

interface EnhancedTableProps {
    classes: ReturnType<typeof useStyles>;
    onRequestSort: (
        event: React.MouseEvent<unknown>,
        property: keyof Data
    ) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

// for the header, allows for sorting when you click on column names
function EnhancedTableHead(props: EnhancedTableProps) {
    const { classes, order, orderBy, rowCount, onRequestSort } = props;
    const createSortHandler = (property: keyof Data) => (
        event: React.MouseEvent<unknown>
    ) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {columns.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? "right" : "left"}
                        padding={
                            headCell.disablePadding ? "none" : "default"
                        }
                        sortDirection={
                            orderBy === headCell.id ? order : false
                        }
                    >
                        {headCell.id !== "image" && (
                            <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={
                                    orderBy === headCell.id ? order : "asc"
                                }
                                onClick={createSortHandler(headCell.id)}
                            >
                                {headCell.label}
                                {orderBy === headCell.id ? (
                                    <span
                                        className={classes.visuallyHidden}
                                    >
                                        {order === "desc"
                                            ? "sorted descending"
                                            : "sorted ascending"}
                                    </span>
                                ) : null}
                            </TableSortLabel>
                        )}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

// the rest of the table
export default function InstanceTable(props: any) {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [order, setOrder] = React.useState<Order>("asc");
    const [orderBy, setOrderBy] = React.useState<keyof Data>("commonName");

    if (props.instances == null) {
        return <h1></h1>;
    }

    const rows = props.instances.map((val: any) => {
        return createData(
            val.image !== null ? val.image : default_img,
            val.commonName,
            val.family,
            val.genus,
            val.location,
            val.id
        );
    });

    // handles clicking of table buttons
    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof Data
    ) => {
        const isAsc = orderBy === property && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(property);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.root}>
                <Table aria-label="table">
                    <EnhancedTableHead
                        classes={classes}
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                    />
                    <TableBody>
                        {/* Sorts according to settings and shows rows */}
                        {stableSort(rows, getComparator(order, orderBy))
                            .slice(
                                page * rowsPerPage,
                                page * rowsPerPage + rowsPerPage
                            )
                            .map((row: any) => {
                                return (
                                    <TableRow
                                        hover
                                        role="checkbox"
                                        tabIndex={-1}
                                    >
                                        {columns.map(column => {
                                            const value = row[column.id];
                                            return (
                                                (column.id === "image" && (
                                                    <TableCell
                                                        key={column.id}
                                                        align={
                                                            column.align
                                                        }
                                                    >
                                                        <img
                                                            src={value}
                                                            style={{
                                                                width:
                                                                    "auto",
                                                                height:
                                                                    "50px"
                                                            }}
                                                            alt="model"
                                                        />
                                                    </TableCell>
                                                )) ||
                                                (column.id ===
                                                    "commonName" &&
                                                    props.type ===
                                                        "plant" && (
                                                        <TableCell
                                                            key={column.id}
                                                            align={
                                                                column.align
                                                            }
                                                        >
                                                            <Link
                                                                to={`/plant/${row.id}`}
                                                            >
                                                                {value}
                                                            </Link>
                                                        </TableCell>
                                                    )) ||
                                                (column.id ===
                                                    "commonName" &&
                                                    props.type ===
                                                        "animal" && (
                                                        <TableCell
                                                            key={column.id}
                                                            align={
                                                                column.align
                                                            }
                                                        >
                                                            <Link
                                                                to={`/animal/${row.id}`}
                                                            >
                                                                {value}
                                                            </Link>
                                                        </TableCell>
                                                    )) || (
                                                    <TableCell
                                                        key={column.id}
                                                        align={
                                                            column.align
                                                        }
                                                    >
                                                        {value}
                                                    </TableCell>
                                                )
                                            );
                                        })}
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>
    );
}
