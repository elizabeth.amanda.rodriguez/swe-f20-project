import React from "react";
import {
    createStyles,
    makeStyles,
    useTheme,
    Theme
} from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Chip from "@material-ui/core/Chip";
import Input from "@material-ui/core/Input";

// styles for filter bars, etc.
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 300,
            maxWidth: 400
        },
        chips: {
            display: "flex",
            flexWrap: "wrap"
        },
        chip: {
            margin: 2
        },
        noLabel: {
            marginTop: theme.spacing(3)
        }
    })
);

// sets style for filter menu drop downs
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250
        }
    }
};

// styles each menu item for filtering
function getStyles(name: string, personName: string[], theme: Theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium
    };
}

// creates the filter drop down
export default function FilterBar(props: any) {
    const classes = useStyles();
    const theme = useTheme();

    if (props.vals == null) {
        return <h1>Could not load filter bar</h1>;
    }

    return (
        <FormControl className={classes.formControl}>
            <InputLabel>{props.vals.name}</InputLabel>
            <Select
                multiple
                value={props.vals.value}
                onChange={props.vals.change}
                input={<Input />}
                renderValue={selected => (
                    <div className={classes.chips}>
                        {(selected as string[]).map(value => (
                            <Chip
                                key={value}
                                label={value}
                                className={classes.chip}
                            />
                        ))}
                    </div>
                )}
                MenuProps={MenuProps}
            >
                {props.vals.vals.map((name: any) => (
                    <MenuItem
                        key={name}
                        value={name}
                        style={getStyles(name, props.vals.value, theme)}
                    >
                        {name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}
