import React from "react";
import { Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import { deepOrange } from "@material-ui/core/colors";

// styles for tools cards
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            paddingLeft: "1em",
            paddingRight: "1em",
            paddingBottom: "2em",
            display: "flex",
            flexDirection: "row"
        },
        header: {
            paddingTop: "0.5em",
            paddingLeft: "1em",
            paddingRight: "1em"
        },
        avatarColor: {
            color: theme.palette.getContrastText(deepOrange[500]),
            backgroundColor: deepOrange[500]
        }
    })
);

// list of apis
const apis = [
    {
        link: "https://trefle.io/",
        primary: "Trefle.io",
        secondary:
            "Trefle is an API that has " +
            "a plethora of information about plants, " +
            "including scientific name, common name, " +
            "edible status, height, family, genus, etc. " +
            "We accessed information from it by making " +
            "GET calls with specific plant names. We " +
            "scraped it with a Python script after " +
            "scraping NatureServe to search up plants " +
            "by scientific name. We then stored the " +
            "information that came back into our " +
            "database.",
        avatar:
            "https://ph-files.imgix.net/10bb6961-8562-4487-958b-733d717597fa?auto=format"
    },
    {
        link: "https://explorer.natureserve.org/api-docs/",
        primary: "NatureServe",
        secondary:
            "NatureServe is an API that has " +
            "information about plants, animals, and " +
            "ecosystems. We used it to gather location " +
            "information about plants (something Trefle " +
            "doesn't have), and to gather information " +
            "about animals, including stats about " +
            "every animal's scientific name, common " +
            "name, geographic location, population " +
            "size, endangered status, etc. We scraped " +
            "it with a Python script that generated " +
            "random animal id numbers and stored the " +
            "result into our database using SQL.",
        avatar:
            "https://www.programmableweb.com/sites/default/files/styles/facebook" +
            "_scale_width_200/public/NatureServe%20API.jpg?itok=-4iAG5lq"
    },
    {
        link: "https://www.geonames.org/export/web-services.html",
        primary: "GeoNames",
        secondary:
            "GeoNames is an API that " +
            "provides basic information about states " +
            "and provinces globally. We made a GET " +
            "request with the state name, and the API " +
            "returned the country name, latitude, " +
            "longitude, population, etc. Once we set up " +
            "our own database, we scraped the info " +
            "with a Python script that sent GET requests " +
            "to the API and stored the resulting info " +
            "into our database columns.",
        avatar:
            "https://symbols-electrical.getvecta.com/stencil_81/" +
            "24_geonames-icon.4df5367b33.svg"
    },
    {
        link: "https://developers.google.com%20/custom-search/v1/overview",
        primary: "Google Search API",
        secondary:
            "We used Google's Custom Search " +
            "API to make image requests to get images " +
            "for plants and animals. We scraped it " +
            "using a Python script that passed in the " +
            "plant or animal common name as a search " +
            "term and compiled the results into an array " +
            "to put in our database.",
        avatar:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/" +
            "Google_%22G%22_Logo.svg/1024px-Google_%22G%22_Logo.svg.png"
    }
];

// list of tools
const tools = [
    {
        primary: "Postman",
        secondary:
            "Used to design our API and make calls to APIs to gather " +
            "information. We documented our API using this tool.",
        img:
            "https://www.postman.com/assets/logos/postman-logo-stacked.svg",
        link: "https://www.postman.com/"
    },
    {
        primary: "Elastic Beanstalk",
        secondary:
            "Deployed our website. Serves our React frontend from our Flask backend",
        img:
            "https://miro.medium.com/max/402/1*zOPbEexteQJIN3TMVTr-Eg.png",
        link: "https://aws.amazon.com/elasticbeanstalk/"
    },
    {
        primary: "Flask",
        secondary:
            "Used to hold our API endpoints and talk to our database. Serves our front end.",
        img:
            "https://seeklogo.com/images/F/flask-logo-44C507ABB7-seeklogo.com.png",
        link: "https://flask.palletsprojects.com/en/1.1.x/"
    },
    {
        primary: "RazorSQL",
        secondary:
            "Used to visualize our database so we could see the contents of it. " +
            "We also used it to create our tables.",
        img:
            "https://static.macupdate.com/products/22299/l/razorsql-logo.webp?v=1605021501",
        link:
            "https://razorsql.com/index.html?adid=jq11&gclid=Cj0KCQiA-rj9BRCAARIsANB_4AAo2A-" +
            "dxrlCjQq6jSAj54sAYc3IB5ePV94u_SC0e7f2IzRBQjupOAIaAjPWEALw_wcB"
    },
    {
        primary: "Material UI",
        secondary:
            "Used to build and design much of our front end. " +
            "Provided pre-designed components we used in our website.",
        img: "https://material-ui.com/static/logo.png",
        link: "https://material-ui.com/"
    },
    {
        primary: "AWS RDS",
        secondary: "Used to host our PostgreSQL database.",
        img:
            "https://d1.awsstatic.com/icons/jp/rds_icon_concole.fe14dd124ff0ce" +
            "7cd8f55f63e0112170c35885f1.png",
        link: "https://aws.amazon.com/rds/"
    },
    {
        primary: "Jest",
        secondary:
            "Used to create unit test of the " +
            "components of the website. This framework made " +
            "it easier to create unit tests for this " +
            "particular part of the project.",
        img:
            "https://seeklogo.com/images/J/jest-logo-F9901EBBF7-seeklogo.com.png",
        link: "https://jestjs.io/"
    },
    {
        primary: "Selenium",
        secondary:
            "Used to create acceptance tests " +
            "of the website GUI. Automated the testing " +
            "process by recording the clicks and inputs " +
            "that happened on our website.",
        img:
            "https://www.selenium.dev/images/selenium_logo_square_green.png",
        link: "https://www.selenium.dev/"
    },
    {
        primary: "Docker",
        secondary:
            "We defined a docker image in the " +
            "Dockerfile to include all of the requirements " +
            "for our website.  It's set up for development " +
            "and production.",
        img:
            "https://www.docker.com/sites/default/files/" +
            "d8/styles/role_icon/public/2019-07/Moby-logo.png?itok=sYH_JEaJ",
        link: "https://www.docker.com/"
    },
    {
        primary: "Mapbox",
        secondary:
            "We used Mapbox to generate maps for " +
            "our model pages. We just passed in a latitude " +
            "and longitude from the model pages to create " +
            "the map.",
        img:
            "https://seeklogo.com/images/M/mapbox-logo-D6FDDD219C-seeklogo.com.png",
        link: "https://www.mapbox.com/"
    },
    {
        primary: "Algolia",
        secondary:
            "Used to implement searching. Algolia " +
            "made it easier to have pagination on our search " +
            "pages, search through our database and render " +
            "results, and highlight relevant terms.",
        img:
            "https://marketplace.magento.com/media/catalog/product/cache" +
            "/7230773f37a543ef738e324844b23ad1/a/l/algoliabadgemagento_3.jpg",
        link: "https://www.algolia.com/"
    },
    {
        primary: "FusionCharts",
        secondary:
            "Used to make our visualizations. FusionCharts provides simple " +
            "ways to create interactive graphics using React.",
        img:
            "https://img.stackshare.io/service/10992/hGbVCEWN_400x400.jpg",
        link: "https://www.fusioncharts.com/"
    }
];

// creates a component that allows list items to be linked
function ListItemLink(props: any) {
    return <ListItem button component="a" {...props} />;
}

export default function AboutTools() {
    const classes = useStyles();
    return (
        <>
            <div className={classes.container}>
                <div style={{ paddingRight: "0.5em", flex: 1 }}>
                    <Card variant="outlined" color="secondary">
                        <Typography
                            className={classes.header}
                            variant="h6"
                            align="left"
                            color="inherit"
                        >
                            Our APIs (Click text to follow link)
                        </Typography>
                        <Divider />
                        <List>
                            {apis.map(api => {
                                return (
                                    <ListItem>
                                        <ListItemAvatar>
                                            <Avatar
                                                src={api.avatar}
                                            ></Avatar>
                                        </ListItemAvatar>
                                        <ListItemLink href={api.link}>
                                            <ListItemText
                                                primary={api.primary}
                                                secondary={api.secondary}
                                            />
                                        </ListItemLink>
                                    </ListItem>
                                );
                            })}
                        </List>
                    </Card>
                </div>

                <div style={{ flex: 1, paddingLeft: "0.5em" }}>
                    <Card variant="outlined" color="secondary">
                        <Typography
                            className={classes.header}
                            variant="h6"
                            align="left"
                            color="inherit"
                        >
                            Our Tools
                        </Typography>
                        <Divider />
                        <List>
                            {tools.map(val => {
                                return (
                                    <ListItem>
                                        <ListItemAvatar>
                                            <Avatar src={val.img}></Avatar>
                                        </ListItemAvatar>
                                        <ListItemLink href={val.link}>
                                            <ListItemText
                                                primary={val.primary}
                                                secondary={val.secondary}
                                            />
                                        </ListItemLink>
                                    </ListItem>
                                );
                            })}
                        </List>
                    </Card>
                </div>
            </div>
        </>
    );
}
