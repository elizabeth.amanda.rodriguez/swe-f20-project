import React from "react";
import { Typography } from "@material-ui/core";
import PersonCard from "./PersonCard";
import Gitlab from "./gitlab-stats";
import Link from "@material-ui/core/Link";
import AboutTools from "./AboutTools";
import AboutStats from "./AboutStats";

// About page
export default class About extends React.Component<
    {},
    { members: any; aggregateStats: any }
> {
    constructor(props: any) {
        super(props);
        // basic team member bios
        let teamMembers = {
            lauren: {
                name: "Lauren Jernigan",
                bio:
                    "I'm Lauren, a junior. I like cats, code, and cooking.",
                responsibilities: "Postman, Database, Docker",
                image: "lauren.jpg",
                commits: 0,
                issues: 0,
                unitTests: 25,
                leader: "Phase 2",
                linkedin: "https://www.linkedin.com/in/lauren-jernigan/"
            },
            elizabeth: {
                name: "Elizabeth Rodriguez",
                bio:
                    "I'm a junior majoring in computer science and I enjoy" +
                    "programming, aerial arts, and painting.",
                responsibilities: "Front end, tests, CI",
                image: "elizabeth.jpg",
                commits: 0,
                issues: 0,
                unitTests: 35,
                leader: "Phase 3",
                linkedin:
                    "https://www.linkedin.com/in/elizabethamandarodriguez/"
            },
            varshinee: {
                name: "Varshinee Sreekanth",
                bio:
                    "Hey there! I'm Varshinee, a junior who loves art," +
                    "coding, and cartoons.",
                responsibilities: "Front end, Filtering, Sorting",
                image: "varshinee.jpeg",
                commits: 0,
                issues: 0,
                unitTests: 20,
                leader: "Phase 4",
                linkedin: "https://www.linkedin.com/in/varshinee/"
            },
            nina: {
                name: "Nina Ahmed",
                bio:
                    "I’m Nina! I like coding, painting, and playing the" +
                    "guitar.",
                responsibilities: "Flask, Database, Deployment, Searching",
                image: "nina.jpg",
                commits: 0,
                issues: 0,
                unitTests: 0,
                leader: "Phase 1",
                linkedin: "https://www.linkedin.com/in/nina-ahmed/"
            },
            erica: {
                name: "Erica Martinez",
                bio:
                    "I'm Erica and I'm interested in data, code, and planners!",
                responsibilities:
                    "User stories, Technical Report, Testing",
                image: "erica.jpg",
                commits: 0,
                issues: 0,
                unitTests: 36,
                leader: "All phases!",
                linkedin:
                    "https://www.linkedin.com/in/erica-martinez-fernandez/"
            }
        };
        this.state = {
            members: teamMembers,
            aggregateStats: {
                totalCommits: 0,
                totalIssues: 0,
                totalUnitTests: 0
            }
        };
    }

    // waits for results of API call
    componentDidMount = async () => {
        let result = await Gitlab.getStats(this.state.members);
        this.setState(
            {
                members: result.members,
                aggregateStats: result.allStats
            },
            () => this.forceUpdate()
        );
    };

    render() {
        return (
            <>
                <br />
                <Typography variant="h3" align="center" color="inherit">
                    About Us
                </Typography>
                <br />
                <Typography
                    variant="body1"
                    align="center"
                    color="inherit"
                    style={{
                        paddingLeft: "8em",
                        paddingRight: "8em"
                    }}
                >
                    We are looking to provide information about native
                    plants and the wildlife that depends on them. Native
                    plants are extremely important for ecosystems, as they
                    provide shelter for countless wildlife and contribute
                    to the food chain.
                </Typography>
                <br />
                <Link href="https://gitlab.com/elizabeth.amanda.rodriguez/swe-f20-project">
                    Gitlab Repo
                </Link>
                <br />
                <Link href="https://documenter.getpostman.com/view/12799338/TVewYPbH">
                    Postman API
                </Link>

                {/* Displays the team members */}
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        paddingLeft: "1em",
                        paddingRight: "1em",
                        paddingBottom: "1em",
                        paddingTop: "1.5em"
                    }}
                >
                    {Object.keys(this.state.members).map((key: any) => {
                        return (
                            <PersonCard member={this.state.members[key]} />
                        );
                    })}
                </div>
                <br />
                <AboutStats aggregateStats={this.state.aggregateStats} />
                <br />
                <AboutTools />
            </>
        );
    }
}
