import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import MLink from "@material-ui/core/Link";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxWidth: 345
        },
        media: {
            height: 0,
            paddingTop: "120%"
        }
    })
);

// shows info for one person for the About page
export default function PersonCard(props: any) {
    const classes = useStyles();

    return (
        <Card className={classes.root} style={{ flex: 1 }}>
            <CardHeader
                title={props.member.name}
                titleTypographyProps={{ variant: "h6" }}
            />
            <CardMedia
                className={classes.media}
                image={require("./../../img/" + props.member.image)}
            />
            <CardContent>
                <Typography
                    variant="body1"
                    color="textPrimary"
                    component="p"
                >
                    {props.member.bio}
                </Typography>
                <br />
                <Typography
                    variant="body1"
                    color="textPrimary"
                    component="p"
                >
                    <MLink href={props.member.linkedin}>LinkedIn</MLink>
                </Typography>
                <br />
                <Typography
                    variant="body1"
                    color="textSecondary"
                    component="p"
                >
                    Responsibilities: {props.member.responsibilities}
                </Typography>
                <br />
                <Typography
                    variant="body1"
                    color="textPrimary"
                    component="p"
                >
                    Team Leader: {props.member.leader}
                </Typography>
                <br />
                <Typography
                    variant="body1"
                    color="textPrimary"
                    component="p"
                >
                    Gitlab Commits: {props.member.commits}
                </Typography>
                <Typography
                    variant="body1"
                    color="textPrimary"
                    component="p"
                >
                    Gitlab Issues: {props.member.issues}
                </Typography>
                <Typography
                    variant="body1"
                    color="textPrimary"
                    component="p"
                >
                    Unit Tests: {props.member.unitTests}
                </Typography>
            </CardContent>
        </Card>
    );
}
