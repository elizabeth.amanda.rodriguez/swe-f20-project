import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

// styles for stat cards
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        media: {
            height: 0,
            paddingTop: "120%"
        },
        container: {
            paddingLeft: "1em",
            paddingRight: "1em",
            paddingBottom: "1em",
            display: "flex",
            flexDirection: "row"
        }
    })
);

export default function AboutStats(props: any) {
    const classes = useStyles();
    return (
        <>
            {/* Commit Card */}
            <div className={classes.container}>
                <div style={{ paddingRight: "0.5em", flex: 1 }}>
                    <Card variant="outlined" className={classes.root}>
                        <CardHeader
                            title="Total Gitlab Commits"
                            titleTypographyProps={{ variant: "h6" }}
                        />
                        <CardContent>
                            <Typography
                                variant="h3"
                                color="textPrimary"
                                component="p"
                            >
                                {props.aggregateStats
                                    ? props.aggregateStats.totalCommits
                                    : 0}
                            </Typography>
                        </CardContent>
                    </Card>
                </div>

                {/* Issues Card */}
                <div
                    style={{
                        paddingRight: "0.5em",
                        paddingLeft: "0.5em",
                        flex: 1
                    }}
                >
                    <Card variant="outlined" className={classes.root}>
                        <CardHeader
                            title="Total Gitlab Issues"
                            titleTypographyProps={{ variant: "h6" }}
                        />
                        <CardContent>
                            <Typography
                                variant="h3"
                                color="textPrimary"
                                component="p"
                            >
                                {props.aggregateStats
                                    ? props.aggregateStats.totalIssues
                                    : 0}
                            </Typography>
                        </CardContent>
                    </Card>
                </div>

                {/* Unit Tests Card */}
                <div style={{ paddingLeft: "0.5em", flex: 1 }}>
                    <Card variant="outlined" className={classes.root}>
                        <CardHeader
                            title="Total Unit Tests"
                            titleTypographyProps={{ variant: "h6" }}
                        />
                        <CardContent>
                            <Typography
                                variant="h3"
                                color="textPrimary"
                                component="p"
                            >
                                {116}
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
            </div>
        </>
    );
}
