export default {
    async getStats(member) {
        let url =
            "https://gitlab.com/api/v4/projects/elizabeth.amanda.rodriguez%" +
            "2Fswe-f20-project/repository/commits?per_page=100&page=";
        // sets our usernames to get our total commits and stuff
        let nameMap = new Map();
        nameMap.set("elizabeth.amanda.rodriguez@gmail.com", "elizabeth");
        nameMap.set("varshinee@utexas.edu", "varshinee");
        nameMap.set("ninaahmed99@gmail.com", "nina");
        nameMap.set("ljernigan447@gmail.com", "lauren");
        nameMap.set("efernmartinez@gmail.com", "erica");
        nameMap.set("ericamtz31@hotmail.com", "erica");

        let userMap = new Map();
        userMap.set("elizabeth.amanda.rodriguez", "elizabeth");
        userMap.set("varshinee1", "varshinee");
        userMap.set("ninaahmed", "nina");
        userMap.set("ljernigan447", "lauren");
        userMap.set("Junimos", "erica");

        let result = { totalCommits: 0, totalIssues: 0 };

        // gets commit info and updates web page
        let page = 1;
        let cont = true; // continues looping as long as we have more pages

        // gets all commit info
        while (cont) {
            let promise = await fetch(url + page, {
                method: "GET",
                headers: {
                    "PRIVATE-TOKEN": "faRazT_ttZ9Jzi3bE22A"
                }
            })
                .then(resp => resp.json())
                .then(function(data) {
                    if (data.length === 0) {
                        cont = false;
                    } else {
                        page++;
                        let commitMap = new Map();
                        for (var i = 0; i < data.length; i++) {
                            if (!commitMap.has(data[i].author_email)) {
                                commitMap.set(data[i].author_email, 1);
                            } else {
                                commitMap.set(
                                    data[i].author_email,
                                    commitMap.get(data[i].author_email) + 1
                                );
                            }
                        }

                        commitMap.forEach(function(value, key) {
                            const name = nameMap.get(key);

                            member["" + name].commits += value;
                        });

                        result.totalCommits += data.length;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }

        cont = true;
        page = 1;
        //gets issues info and updates web page
        url =
            "https://gitlab.com/api/v4/projects/elizabeth.amanda.rodriguez%" +
            "2Fswe-f20-project/issues?per_page=100&page=";
        while (cont) {
            let issuePromise = await fetch(url + page, {
                method: "GET",
                headers: {
                    "PRIVATE-TOKEN": "faRazT_ttZ9Jzi3bE22A"
                }
            })
                .then(resp => resp.json())
                .then(function(data) {
                    if (data.length === 0) {
                        cont = false;
                    } else {
                        page++;
                        let issueMap = new Map();
                        for (let i = 0; i < data.length; i++) {
                            data[i].assignees.forEach(function(person) {
                                if (!issueMap.has(person.username)) {
                                    issueMap.set(person.username, 1);
                                } else {
                                    issueMap.set(
                                        person.username,
                                        issueMap.get(person.username) + 1
                                    );
                                }
                            });
                        }

                        issueMap.forEach(function(value, key) {
                            const name = userMap.get(key);

                            member["" + name].issues += value;
                        });

                        result.totalIssues += data.length;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
        // unit test stuff
        result.totalUnitTests = 0;
        return { allStats: result, members: member };
    }
};
