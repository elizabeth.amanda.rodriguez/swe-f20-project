import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import Main from "../SitewideComponents/Main";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

jest.mock("mapbox-gl/dist/mapbox-gl", () => ({
    Map: () => ({})
}));

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <BrowserRouter>
            <Main></Main>
        </BrowserRouter>,
        div
    );
});
