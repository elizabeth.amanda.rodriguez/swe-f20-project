import React from "react";
import ReactDOM from "react-dom";
import Visualizations from "../Visualizations/Visualizations";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Visualizations></Visualizations>, div);
});