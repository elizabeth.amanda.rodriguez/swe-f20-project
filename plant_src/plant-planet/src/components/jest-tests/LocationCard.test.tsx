import React from "react";
import ReactDOM from "react-dom";
import About from "../AboutPage/About";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<About></About>, div);
});
