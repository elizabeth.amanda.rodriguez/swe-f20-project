import React from "react";
import ReactDOM from "react-dom";
import PlantTemplateGrid from "../Models/PlantModel/PlantTemplateGrid";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<PlantTemplateGrid></PlantTemplateGrid>, div);
});