import React from "react";
import ReactDOM from "react-dom";
import Filter from "../Models/Filter";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <div data-testid="filter">
            <Filter></Filter>
        </div>,
        div
    );
});
