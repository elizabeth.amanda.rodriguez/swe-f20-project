import React from "react";
import ReactDOM from "react-dom";
import PlantTemplate from "../Models/PlantModel/PlantTemplate";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

jest.mock("mapbox-gl/dist/mapbox-gl", () => ({
    Map: () => ({})
}));

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<PlantTemplate></PlantTemplate>, div);
});
