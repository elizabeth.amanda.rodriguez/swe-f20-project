import React from "react";
import ReactDOM from "react-dom";
import HomeTop from "../SplashPage/HomeTop";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import leaves from "../../img/leaves-background.jpg";

const slideImages = [leaves];

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<HomeTop></HomeTop>, div);
});

it("renders HomeTop with a background", () => {
    const { getByTestId } = render(<HomeTop></HomeTop>);
    const background = getByTestId("home-bg-image");

    expect(background).toHaveStyle({
        backgroundImage: `url(${slideImages[0]})`
    });
});
