import React from "react";
import ReactDOM from "react-dom";
import AnimalModel from "../Models/AnimalModel/AnimalModel";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<AnimalModel></AnimalModel>, div);
});

// it("renders animal model with a slideshow title", () =>{
//     const {getByTestId} = render(<AnimalModel></AnimalModel>);
//     const animalModel = getByTestId('animal-model');
//     const title = getByTestId('animal-title');

//     expect(animalModel).toContainElement(title);
// })

// it("renders splash with a slideshow", () =>{
//     const {getByTestId} = render(<Splash></Splash>);
//     const animalModel = getByTestId('animal-model');
//     const table = getByTestId('table');

//     expect(animalModel).toContainElement(table);
// })
