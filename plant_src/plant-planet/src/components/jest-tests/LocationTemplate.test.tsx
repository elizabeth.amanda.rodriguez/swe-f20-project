import React from "react";
import ReactDOM from "react-dom";
import LocationTemplate from "../Models/LocationModel/Template Page/LocationTemplate";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

jest.mock("mapbox-gl/dist/mapbox-gl", () => ({
    Map: () => ({})
}));

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<LocationTemplate></LocationTemplate>, div);
});
