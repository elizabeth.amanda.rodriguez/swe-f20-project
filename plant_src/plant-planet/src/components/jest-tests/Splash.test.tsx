import React from "react";
import ReactDOM from "react-dom";
import Splash from "../SplashPage/Splash";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Splash></Splash>, div);
});

it("renders splash with a slideshow title", () => {
    const { getByTestId } = render(<Splash></Splash>);
    const splash = getByTestId("splash");
    const title = getByTestId("slides-title");

    expect(splash).toContainElement(title);
});

it("renders splash with a slideshow", () => {
    const { getByTestId } = render(<Splash></Splash>);
    const splash = getByTestId("splash");
    const pptx = getByTestId("slideshow");

    expect(splash).toContainElement(pptx);
});
