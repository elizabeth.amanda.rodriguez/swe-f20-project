import React from "react";
import ReactDOM from "react-dom";
import NavBar from "../SitewideComponents/NavBar";
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <BrowserRouter>
            <NavBar></NavBar>
        </BrowserRouter>,
        div
    );
});
