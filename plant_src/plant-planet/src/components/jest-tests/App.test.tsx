import React from "react";
import ReactDOM from "react-dom";
import App from "../SitewideComponents/App";
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";

jest.mock("mapbox-gl/dist/mapbox-gl", () => ({
    Map: () => ({})
}));

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
        <BrowserRouter>
            <App></App>
        </BrowserRouter>,
        div
    );
});
