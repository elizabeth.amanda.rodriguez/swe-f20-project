import React from "react";
import "../../css/styles.css";
import leaves from "../../img/leaves-background.jpg";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const slideImages = [leaves];

// top part of home page with summary of our purpose
const HomeTop = () => {
    return (
        <div className="top-container">
            <div className="top-pic">
                <div
                    data-testid="home-bg-image"
                    style={{ backgroundImage: `url(${slideImages[0]})` }}
                >
                    <Grid container spacing={3} justify="center">
                        <Grid item xs={12}>
                            <div
                                data-testid="home-title"
                                className="top-h"
                            >
                                NATIVE PLANTS ARE AN IMPORTANT PART OF
                                LOCAL ECOSYSTEMS AND SUPPORT NATIVE
                                WILDLIFE.
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <div className="top-motive">
                                A website looking to provide information
                                about native plants and the wildlife that
                                depends on them. Native plants are
                                extremely important for ecosystems, as they
                                provide shelter for countless wildlife and
                                contribute to the food chain.
                            </div>
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                variant="contained"
                                color="primary"
                                href="/location"
                            >
                                Find plants where I live
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </div>
    );
};

export default HomeTop;
