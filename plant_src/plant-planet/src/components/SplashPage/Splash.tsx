import React from "react";
import Slideshow from "../SitewideComponents/Slideshow";
import HomeTop from "./HomeTop";
import plant from "../../img/default_plant.png";
import animal from "../../img/default_animal.jpg";
import location from "../../img/default-location.jpg";

export default function Splash() {
    const slideImages = [
        { image: plant, name: "Plants" },
        { image: animal, name: "Animals" },
        { image: location, name: "Locations" }
    ];
    //displays the splash page with a slideshow
    return (
        <div data-testid="splash">
            <HomeTop></HomeTop>
            <br></br>
            <br></br>
            <h1 data-testid="slides-title">Our models</h1>
            <br></br>
            <Slideshow images={slideImages}></Slideshow>
        </div>
    );
}
