import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import React from "react";

// NOTE: THIS VISUALIZATION IS NOT FINISHED AND WILL BE COMPLETED BY PRESENTATION DAY

// Resolves charts dependancy
charts(FusionCharts);

class ReqPieGraph extends React.Component<{}, { dataSource: any }> {
    constructor(props: any) {
        super(props);
        this.state = {
            dataSource: {
                chart: {}
            }
        };
    }

    // makes the api request
    async componentDidMount() {
        var ferf = await fetch("https://utplannerplus.me/api/Requirements/?page_number=1&page_size=30&filter_by=&filter_string=&sort_by=&reverse=False")
            .then(res => res.json())
            .then(data => {
                let reqArray = data.data;
                let reqMap = new Map();
                reqArray.forEach(function(val) {
                    let reqData = val.typeOfRequirement;
                    if (!reqMap.has(reqData)) {
                        reqMap.set(reqData, 1);
                    } else {
                        reqMap.set(reqData, reqMap.get(reqData) + 1);
                    }
                });
                
                let reqs: { label: string; value: string }[] = [];
                reqMap.forEach(function(value, key) {
                    reqs.push({
                        label: key,
                        value: value + ""
                    });
                });

                this.setState({
                    dataSource: {
                        chart: {
                            caption: "Classes for each requirement",
                            plottooltext:
                                "<b>$percentValue</b> of the classes fulfil the $label requirement",
                            showlegend: "1",
                            showpercentvalues: "1",
                            legendposition: "bottom",
                            usedataplotcolorforlabels: "1",
                            theme: "ocean"
                        },
                        data: reqs
                    }
                });
                return reqs;
            });
    }
    render() {
        return (
            <ReactFusioncharts
                type="pie2d"
                width="800"
                height="500"
                dataFormat="JSON"
                dataSource={this.state.dataSource}
            />
        );
    }
}

export default ReqPieGraph;
