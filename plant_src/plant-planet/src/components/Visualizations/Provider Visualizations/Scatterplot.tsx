import React from "react";
import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

ReactFusioncharts.fcRoot(FusionCharts, charts, FusionTheme);

// Resolves charts dependancy
charts(FusionCharts);

// creates our location scatterplot
class Scatterplot extends React.Component<{}, { dataSource: any }> {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: {}
        };
    }

    // makes api request and builds data
    async componentDidMount() {
        var promise = await fetch(
            "https://utplannerplus.me/api/Professors/?page_number=1&page_size=200&filter_by=&filter_string=&sort_by=&reverse=False"
        )
            .then(res => res.json())
            .then(data => {
                let dataset = [
                    {
                        seriesname: "Ratings",
                        anchorbgcolor: "5D62B5",
                        data: new Array<Object>([])
                    }
                ];

                data.data.forEach(item => {
                    if (
                        item.numRatings != "N/A" &&
                        item.overallRating != "N/A"
                    ) {
                        dataset[0].data.push({
                            x: item.numRatings,
                            y: item.overallRating
                        });
                    }
                });

                // sets the data structure fo chart rendering
                this.setState({
                    dataSource: {
                        chart: {
                            caption:
                                "Average Rating of Professors vs. Total Number of Rating",
                            subcaption:
                                "Does the number of ratings skew the average?",
                            xaxisname: "Number of Ratings",
                            yaxisname: "Average Rating",
                            xaxisminvalue: "1",
                            xaxismaxvalue: "60",
                            ynumberprefix: "",
                            yaxisminvalue: "1",
                            xnumbersuffix: "",
                            theme: "fusion",
                            showlegend: "0",
                            plottooltext:
                                "<b>$yDataValue</b> is the average rating <br>when this professor has " +
                                "<b>$xDataValue</b> ratings."
                        },
                        categories: [
                            {
                                verticallinedashed: "1",
                                verticallinedashlen: "1",
                                verticallinedashgap: "1",
                                verticallinethickness: "1",
                                verticallinecolor: "#000000",
                                category: [
                                    {
                                        x: "1",
                                        label: "1",
                                        showverticalline: "0"
                                    },
                                    {
                                        x: "10",
                                        label: "10"
                                    },
                                    {
                                        x: "20",
                                        label: "20"
                                    },
                                    {
                                        x: "35",
                                        label: "35"
                                    },
                                    {
                                        x: "50",
                                        label: "50"
                                    }
                                ]
                            }
                        ],
                        dataset: dataset
                    }
                });
            });
    }

    render() {
        return (
            <ReactFusioncharts
                type="scatter"
                width="700"
                height="700"
                dataFormat="JSON"
                dataSource={this.state.dataSource}
            />
        );
    }
}
export default Scatterplot;
