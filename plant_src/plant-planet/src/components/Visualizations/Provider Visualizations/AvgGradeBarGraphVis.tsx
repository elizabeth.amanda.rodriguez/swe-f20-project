import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import React from "react";

// NOTE: THIS VISUALIZATION IS NOT FINISHED AND WILL BE COMPLETED BY PRESENTATION DAY

// Resolves charts dependancy
charts(FusionCharts);

class AvgGradeVis extends React.Component<{}, { dataSource: any }> {
    constructor(props: any) {
        super(props);
        this.state = {
            dataSource: {
                chart: {}
            }
        };
    }

    async componentDidMount() {
        var ferf = await fetch("https://utplannerplus.me/api/Courses/?page_number=1&page_size=300&filter_by=&filter_string=&sort_by=&reverse=False")
            .then(res => res.json())
            .then(data => {
                let reqArray = data.data;
                let reqMap = new Map();
                reqArray.forEach(function(val) {
                    let reqData = val.days;
                    reqData.forEach(function(day) {
                        if (!reqMap.has(day)) {
                            reqMap.set(day, 1);
                        } else {
                            reqMap.set(day, reqMap.get(day) + 1);
                        }
                    });
                });
                
                let reqs: { label: string; value: string }[] = [];
                reqMap.forEach(function(value, key) {
                    reqs.push({
                        label: key,
                        value: value + ""
                    });
                });

                this.setState({
                    dataSource: {
                        chart: {
                            caption: "Days the Courses Are Offered On",
                            plottooltext:
                                "<b>$value</b> of the classes are offered on $label",
                            showlegend: "1",
                            showpercentvalues: "1",
                            legendposition: "bottom",
                            usedataplotcolorforlabels: "1",
                            theme: "fusion"
                        },
                        data: reqs
                    }
                });
                return reqs;
            });
    }

    render() {
        return (
            <ReactFusioncharts
                type="bar2d"
                width="700"
                height="700"
                dataFormat="JSON"
                dataSource={this.state.dataSource}
            />
        );
    }
}

export default AvgGradeVis;
