import React from "react";
import AvgGradeVis from "./Provider Visualizations/AvgGradeBarGraphVis";
import Scatterplot from "./Provider Visualizations/Scatterplot";
import ReqPieChart from "./Provider Visualizations/ReqPieChart";
import { Typography } from "@material-ui/core";

// NOTE: THIS PAGE IS NOT FINISHED AND WILL BE COMPLETED BY PRESENTATION DAY
// Please ignore style issues and unused imports for this file

// Page for all our provider visualizations
export default function OtherVisualizations(props: any) {
    return (
        <div>
            <br />
            <br />
            <Typography variant="h3" align="center" color="inherit">
                Provider Visualizations
            </Typography>
            <br />
            <Scatterplot></Scatterplot>
            <AvgGradeVis></AvgGradeVis>
            <ReqPieChart></ReqPieChart>
        </div>
    );
}
