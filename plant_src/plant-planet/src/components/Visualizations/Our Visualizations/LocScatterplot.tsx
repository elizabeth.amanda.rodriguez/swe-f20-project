import React from "react";
import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

ReactFusioncharts.fcRoot(FusionCharts, charts, FusionTheme);

// Resolves charts dependancy
charts(FusionCharts);

// creates our location scatterplot
class Scatterplot extends React.Component<{}, { dataSource: any }> {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: {}
        };
    }

    // makes api request and builds data
    async componentDidMount() {
        var promise = await fetch("/api/getlocations")
            .then(res => res.json())
            .then(data => {
                let dataset = [
                    {
                        seriesname: "Population",
                        anchorbgcolor: "5D62B5",
                        data: new Array<Object>([])
                    }
                ];

                Object.keys(data).map(key => {
                    dataset[0].data.push({
                        x: data[key].latitude,
                        y: data[key].population
                    });
                });

                this.setState({
                    dataSource: {
                        chart: {
                            caption:
                                "Number of People Living at Certain Latitudes",
                            subcaption: "Does latitude affect population?",
                            xaxisname: "Latitude",
                            yaxisname: "Population",
                            xaxisminvalue: "20",
                            xaxismaxvalue: "65",
                            ynumberprefix: "",
                            yaxisminvalue: "150000",
                            xnumbersuffix: "",
                            theme: "fusion",
                            showlegend: "0",
                            plottooltext:
                                "<b>$yDataValue</b> is the population <br>at " +
                                "<b>$xDataValue</b> latitude"
                        },
                        categories: [
                            {
                                verticallinedashed: "1",
                                verticallinedashlen: "1",
                                verticallinedashgap: "1",
                                verticallinethickness: "1",
                                verticallinecolor: "#000000",
                                category: [
                                    {
                                        x: "20",
                                        label: "20°",
                                        showverticalline: "0"
                                    },
                                    {
                                        x: "32",
                                        label: "32°"
                                    },
                                    {
                                        x: "44",
                                        label: "44°"
                                    },
                                    {
                                        x: "56",
                                        label: "56°"
                                    },
                                    {
                                        x: "68",
                                        label: "68°"
                                    }
                                ]
                            }
                        ],
                        dataset: dataset
                    }
                });
            });
    }

    render() {
        return (
            <ReactFusioncharts
                type="scatter"
                width="700"
                height="700"
                dataFormat="JSON"
                dataSource={this.state.dataSource}
            />
        );
    }
}
export default Scatterplot;
