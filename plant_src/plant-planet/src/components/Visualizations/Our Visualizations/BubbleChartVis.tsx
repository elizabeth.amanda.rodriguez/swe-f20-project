import React from "react";
import * as d3 from "d3";

// creates a d3 visualization of bubbles for locations
class BubbleChartVis extends React.Component<{}, { locData: any }> {
    constructor(props: any) {
        super(props);
        this.state = {
            locData: {}
        };
        this.createBubbleChart = this.createBubbleChart.bind(this);
    }

    // makes api request for locations
    async componentDidMount() {
        var locData = await fetch("/api/getlocations")
            .then(res => res.json())
            .then(data => {
                let locs: Object[] = Object.keys(data).map(key => {
                    let num = 0;
                    if (data[key].plant_ids != null) {
                        num += data[key].plant_ids.length;
                    }
                    if (data[key].animal_ids != null) {
                        num += data[key].animal_ids.length;
                    }
                    let temp = {
                        name: data[key].state,
                        value: num
                    };

                    return temp;
                });
                this.setState({
                    locData: locs
                });
                return locs;
            });
        this.createBubbleChart();
    }

    // Link to code example we altered:
    // https://gitlab.com/amirmst/freshman-2-freshgrad/-/blob/master/src/components/StateCompanyBubbleChart.tsx
    createBubbleChart() {
        var json = { children: this.state.locData };

        var diameter = 1000,
            color = d3.scaleOrdinal(d3.schemeSet3);

        var bubble = d3
            .pack()
            .size([diameter, diameter])
            .padding(5);

        var margin = {
            left: 0,
            right: 100,
            top: 0,
            bottom: 0
        };

        var svg = d3
            .select("#locationBubbleChart")
            .append("svg")
            .attr(
                "viewBox",
                "0 0 " + (diameter + margin.right) + " " + diameter
            )
            .attr("width", diameter + margin.right)
            .attr("height", diameter)
            .attr("class", "chart-svg");

        var root: any = d3
            .hierarchy(json)
            .sum(function(d: any) {
                return d.value;
            })
            .sort(function(a: any, b: any) {
                return b.value - a.value;
            });

        bubble(root);

        var node = svg
            .selectAll(".node")
            .data(root.children)
            .enter()
            .append("g")
            .attr("class", "node")
            .attr("transform", function(d: any) {
                return "translate(" + d.x + " " + d.y + ")";
            })
            .append("g")
            .attr("class", "graph");

        node.append("circle")
            .attr("r", function(d: any) {
                return d.r;
            })
            .style("fill", function(d: any) {
                return color(d.data.name);
            });

        node.append("text")
            .attr("dy", ".1em")
            .style("text-anchor", "middle")
            .text(function(d: any) {
                return d.data.name;
            })
            .style("fill", "#000000");
    }
    render() {
        return <div id="locationBubbleChart"></div>;
    }
}

export default BubbleChartVis;
