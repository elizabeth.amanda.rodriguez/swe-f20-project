import React from "react";
import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.powercharts";
import ReactFusioncharts from "react-fusioncharts";

// NOTE: THIS VISUALIZATION IS NOT FINISHED AND WILL BE COMPLETED BY PRESENTATION DAY

// Resolves charts dependancy
charts(FusionCharts);

class Sunburst extends React.Component<{}, { dataSource: any }> {
    constructor(props: any) {
        super(props);
        this.state = {
            dataSource: {
                chart: {
                    caption: "Breakdown of taxonomy",
                    subcaption: "Last Quarter",
                    showplotborder: "1",
                    plotfillalpha: "60",
                    hoverfillcolor: "#CCCCCC",
                    numberprefix: "$",
                    plottooltext:
                        "Sales of <b>$label</b> was <b>$$valueK</b>, which was $percentValue of parent category",
                    theme: "fusion"
                },
                category: []
            }
        };
    }

    async componentDidMount() {
        await fetch("/api/getanimals")
            .then(res => res.json())
            .then(data => {
                let genMap = new Map();
                let genCount = 0;
                let famMap = new Map();
                let famCount = 0;
                let famToGen = new Map();
                let total = 0;
                Object.keys(data).map(key => {
                    // Count the number of each genus, store in map
                    let gen = data[key].phylum;
                    if (!genMap.has(gen)) {
                        genMap.set(gen, 1);
                    } else {
                        ++genCount;
                        genMap.set(gen, genMap.get(gen) + 1);
                    }
                    // Count the number of each family, store in map
                    let fam = data[key].class;
                    if (!famMap.has(fam)) {
                        famMap.set(fam, 1);
                    } else {
                        ++famCount;
                        famMap.set(fam, famMap.get(fam) + 1);
                    }
                    // Map genuses to families. Each family has an array of corresponding genuses
                    let arr;
                    if (famToGen.has(fam)) {
                        arr = famToGen.get(fam);
                    } else {
                        arr = new Array();
                        famToGen.set(fam, arr);
                    }
                    if (!arr.includes(gen)) {
                        arr.push(gen);
                    }
                    // Count the number of families.
                    ++total;
                });
                // Container is held by the top level category.
                let container = new Array();
                // ContMap is an object in container.
                let contMap = {};
                contMap["label"] = "Animals";
                contMap["tooltext"] =
                    "Please hover over a sub-category to see details";
                contMap["color"] = "#ffffff";
                contMap["value"] = "150";
                contMap["category"] = new Array();
                container.push(contMap);
                let baseArr = contMap["category"];
                // Create an object for each family.
                famMap.forEach(function(value, family) { 
                    let fMap = Sunburst.setupMap(
                        family,
                        famMap,
                        total,
                        true
                    );
                    baseArr.push(fMap);
                    let genArr = fMap["category"];
                    // Nest genus objects inside the family.
                    genMap.forEach(function(value, genus) {
                        let gMap = Sunburst.setupMap(
                            genus,
                            genMap,
                            famMap.get(family),
                            false
                        );
                        let firstBase = fMap["category"];
                        firstBase.push(gMap);
                    })
                })
                this.setState(
                    {
                        dataSource: {
                            chart: {
                                caption: "Breakdown of taxonomy",
                                subcaption: "",
                                showplotborder: "1",
                                plotfillalpha: "60",
                                hoverfillcolor: "#CCCCCC",
                                numberprefix: "$",
                                plottooltext:
                                    "<b>$label</b> was $percentValue of parent category",
                                theme: "fusion"
                            },
                            category: container 
                        }
                    },
                    () => this.forceUpdate()
                );
            });
    }

    // Creates the object for the given family or genus.
    static setupMap(name: any, countMap: any, total: any, cat: any) {
        let obj = {};
        obj["label"] = name;
        obj["color"] = "#f8bd19";
        obj["value"] = (countMap.get(name) / total).toPrecision(3).toString();
        if (cat) {
            obj["category"] = new Array();
        } 
        return obj;
    }

    render() {
        return (
            <ReactFusioncharts
                type="multilevelpie"
                width="100%"
                height="100%"
                dataFormat="JSON"
                dataSource={this.state.dataSource}
            />
        );
    }
}

export default Sunburst;
