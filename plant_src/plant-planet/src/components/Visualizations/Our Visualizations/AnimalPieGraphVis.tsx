import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import React from "react";

// Resolves charts dependancy
charts(FusionCharts);

// creates our pie graph of taxonomical info
class PieGraph extends React.Component<{}, { dataSource: any }> {
    constructor(props: any) {
        super(props);
        this.state = {
            dataSource: {}
        };
    }

    // makes the API request to get the animal info
    async componentDidMount() {
        var ferf = await fetch("/api/getanimals")
            .then(res => res.json())
            .then(data => {
                let taxMap = new Map();
                Object.keys(data).map(key => {
                    let tax = data[key].taxonomy;
                    if (!taxMap.has(tax)) {
                        taxMap.set(tax, 1);
                    } else {
                        taxMap.set(tax, taxMap.get(tax) + 1);
                    }
                });
                let taxonomy: { label: string; value: string }[] = [];

                taxMap.forEach(function(value, key) {
                    taxonomy.push({
                        label: key,
                        value: value + ""
                    });
                });

                this.setState({
                    dataSource: {
                        chart: {
                            caption: "The Taxonomy of Our Animals",
                            plottooltext:
                                "<b>$percentValue</b> of our animals are $label",
                            showlegend: "1",
                            showpercentvalues: "1",
                            legendposition: "bottom",
                            usedataplotcolorforlabels: "1",
                            theme: "ocean"
                        },
                        data: taxonomy
                    }
                });
                return taxonomy;
            });
    }

    // creates our chart
    render() {
        return (
            <ReactFusioncharts
                type="pie2d"
                width="800"
                height="500"
                dataFormat="JSON"
                dataSource={this.state.dataSource}
            />
        );
    }
}

export default PieGraph;
