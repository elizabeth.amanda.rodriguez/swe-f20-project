import React from "react";
import BubbleChartVis from "./Our Visualizations/BubbleChartVis";
import PieGraph from "./Our Visualizations/AnimalPieGraphVis";
import ScatterPlot from "./Our Visualizations/LocScatterplot";
import Sunburst from "./Our Visualizations/Sunburst";
import { Typography } from "@material-ui/core";

// Creates our visualizations, using FusionCharts and D3
export default function Visualizations(props: any) {
    return (
        <div>
            <br />
            <Typography variant="h3" align="center" color="inherit">
                Our Visualizations
            </Typography>
            <br />
            <div data-testid="visualizations">
                <PieGraph data-testid="pie"></PieGraph>
                <br />
                <br />
                <Typography variant="h6" align="center" color="inherit">
                    Amount of Plant/Animal Instances for Each Location
                </Typography>
                <BubbleChartVis data-testid="bubble"></BubbleChartVis>
                <ScatterPlot></ScatterPlot>
                {/* <Sunburst></Sunburst> */}
            </div>
            <br />
        </div>
    );
}
