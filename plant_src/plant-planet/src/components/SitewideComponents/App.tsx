import React from "react";
import "../../css/styles.css";
import Main from "./Main";
import NavBar from "./NavBar";

// renders main app
function App() {
    return (
        <div className="App">
            <NavBar />
            <Main />
        </div>
    );
}

export default App;
