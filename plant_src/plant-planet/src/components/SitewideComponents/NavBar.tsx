import React, { useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import SearchBar from "material-ui-search-bar";
import { withRouter } from "react-router-dom";
import logo from "../../img/logo.png";

function NavBar(props: any) {
    const [searchTerm, setSearchTerm] = useState(String);

    const linkStyle = {
        marginRight: 16,
        color: "#ffffff"
    };

    function handleSearch() {
        props.history.push({
            pathname: "/search",
            state: {
                showPlant: true,
                showAnimal: true,
                showLocation: true,
                refinement: searchTerm
            }
        });
    }

    const navbarLinks = [
        {
            link: "/",
            text: "Home"
        },
        {
            link: "/about",
            text: "About"
        },
        {
            link: "/plant",
            text: "Plants"
        },
        {
            link: "/animal",
            text: "Animals"
        },
        {
            link: "/location",
            text: "Locations"
        }
        // ,
        // {
        //     link: "/visualizations",
        //     text: "Visualizations"
        // }
    ];

    const [anchorEl, setAnchorEl] = React.useState(null);  const handleClick = event => {
        setAnchorEl(event.currentTarget);
      };  const handleClose = () => {
        setAnchorEl(null);
      };

    return (
        <>
            {/* Default nav bar for the website */}
            <AppBar position="static" style={{ background: "#EE6055" }}>
                <Toolbar>
                    <Typography
                        variant="h5"
                        align="left"
                        color="inherit"
                        style={{ flex: 1 }}
                    >
                        {/* <MLink href="/">The Plant Planet</MLink> */}
                        <a href="/">
                            <img src={logo} alt="Plant Planet"></img>
                        </a>
                    </Typography>
                    {navbarLinks.map(val => {
                        return (
                            <Button
                                href={val.link}
                                color="secondary"
                                style={linkStyle}
                            >
                                {val.text}
                            </Button>
                        );
                    })}
                    <Button 
                        aria-controls="simple-menu" 
                        aria-haspopup="true" 
                        onClick={handleClick} 
                        color="secondary" 
                        style={linkStyle}
                    >  
                        Visualizations
                    </Button>  
                    <Menu  
                        id="Menu"  
                        anchorEl={anchorEl}  
                        keepMounted  
                        open={Boolean(anchorEl)}  
                        onClose={handleClose}  
                    >  
                        <MenuItem onClick={handleClose}>
                            <a 
                                href="/visualizations" 
                                style={{ textDecoration: 'none', color: 'black' }}
                            >
                                Our Visualizations
                            </a>
                        </MenuItem>  
                        <MenuItem onClick={handleClose}>
                            <a 
                                href="/othervisualizations" 
                                style={{ textDecoration: 'none', color: 'black' }}
                            >
                                Provider Visualizations
                            </a>
                        </MenuItem>  
                    </Menu> 

                    <SearchBar
                        onChange={newValue => setSearchTerm(newValue)}
                        onRequestSearch={handleSearch}
                        style={{ width: "300px" }}
                    />
                </Toolbar>
            </AppBar>
        </>
    );
}

export default withRouter(NavBar);
