import React from "react";
import { Slide } from "react-slideshow-image";
import "../../frontend-files/react-slideshow-image/dist/styles.css";
import defaultImg from "./../../img/default_model_image.jpg";

// generates a slideshow depending on passed in images
const Slideshow = (props: any) => {
    let images = props.images;
    if (images == null) {
        images = [{ image: defaultImg }];
    }
    return (
        <div className="slide-container" data-testid="slideshow">
            <Slide>
                {images.map((val: any) => {
                    return (
                        <div className="each-slide">
                            <div
                                style={{
                                    backgroundImage: `url(${val.image})`
                                }}
                            >
                                {val.name && <span>{val.name}</span>}
                            </div>
                        </div>
                    );
                })}
            </Slide>
        </div>
    );
};

export default Slideshow;
