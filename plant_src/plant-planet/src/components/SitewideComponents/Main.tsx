import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import Splash from "../SplashPage/Splash";
import About from "../AboutPage/About";
import LocationModel from "../Models/LocationModel/Model Page/LocationModel";
import AnimalModel from "../Models/AnimalModel/AnimalModel";
import PlantTemplate from "../Models/PlantModel/PlantTemplate";
import PlantModel from "../Models/PlantModel/PlantModel";
import AnimalTemplate from "../Models/AnimalModel/AnimalTemplate";
import LocationTemplate from "../Models/LocationModel/Template Page/LocationTemplate";
import SearchResults from "./../SearchPage/SearchResults";
import Visualizations from "../Visualizations/Visualizations";
import OtherVisualizations from "../Visualizations/OtherVisualizations";

// Main router for the app
// sets valid page routes to render
const Main = () => (
    <main>
        <Switch>
            <Route exact path="/" component={Splash} />
            <Route path="/about" component={About} />

            <Route exact path="/plant" component={PlantModel} />
            <Route path="/plant/:id" component={PlantTemplate} />

            <Route exact path="/animal" component={AnimalModel} />
            <Route path="/animal/:id" component={AnimalTemplate} />

            <Route exact path="/location" component={LocationModel} />

            <Route path="/location/:id" component={LocationTemplate} />

            <Route exact path="/search" component={SearchResults} />
            <Route
                exact
                path="/visualizations"
                component={Visualizations}
            />
            <Route
                exact
                path="/othervisualizations"
                component={OtherVisualizations}
            />
        </Switch>
    </main>
);

export default Main;
