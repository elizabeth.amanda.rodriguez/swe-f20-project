from flask import Flask, render_template, jsonify
from flask_cors import CORS
import sqlalchemy as db


application = app = Flask(
    __name__,
    static_folder="../plant-planet/build/static",
    template_folder="../plant-planet/build/",
)
CORS(app)
engine = db.create_engine(
    "postgres://nina:SunflowerVol.6@planttest.c8reqlopldwe."
    + "us-east-1.rds.amazonaws.com:5432/postgres"
)


# convert the data returned from the SQL command to an array
def get_col_names(line):
    names = []
    for col in line:
        names += col
    return names


# get column names for each table for any json objects we construct
plant_column_names = get_col_names(
    engine.execute(
        """SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS"""
        + """ WHERE TABLE_NAME=N\'plants\'"""
    )
)
animal_column_names = get_col_names(
    engine.execute(
        """SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS"""
        + """ WHERE TABLE_NAME=N\'animals\'"""
    )
)
location_column_names = get_col_names(
    engine.execute(
        """SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS"""
        + """ WHERE TABLE_NAME=N\'locations\'"""
    )
)


# start the static part of the app from the React build files
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def startapp(path):
    return render_template("index.html")


# return a JSON object with they keys corresponding to plant ids and
# the values as the JSON constructed from the database columns and data
@app.route("/api/getplants")
def get_plant_model():
    plant_names = engine.execute("""SELECT * FROM plants""")
    plant_map = {}
    for p in plant_names:
        plant_json = build_json(p, plant_column_names)
        plant_map[plant_json["plant_id"]] = plant_json
    return jsonify(plant_map)


# return a JSON object with they keys corresponding to animal ids and
# the values as the JSON constructed from the database columns and data
@app.route("/api/getanimals")
def get_animal_model():
    animal_names = engine.execute("""SELECT * FROM animals""")
    animal_map = {}
    for a in animal_names:
        animal_json = build_json(a, animal_column_names)
        animal_map[animal_json["animal_id"]] = animal_json
    return jsonify(animal_map)


# return a JSON object with they keys corresponding to location ids and
# the values as the JSON constructed from the database columns and data
@app.route("/api/getlocations")
def get_location_model():
    location_names = engine.execute("""SELECT * FROM locations""")
    location_map = {}
    for l in location_names:
        location_json = build_json(l, location_column_names)
        location_map[location_json["location_id"]] = location_json
    return jsonify(location_map)


# return a JSON object with they keys corresponding to the columns names for
# the plants table and the values corresponding to the actual data
@app.route("/api/getplant/<plant_id>")
def get_plant(plant_id):
    plant = engine.execute("""SELECT * FROM plants WHERE plant_id=""" 
    + str(plant_id))
    ret_json = build_json(next(iter(plant)), plant_column_names)

    # include the latitude and longitude for one of the locations the plant
    # is in
    locations = ret_json["location_ids"]
    lat = None
    lon = None
    if locations is not None:
        loc = engine.execute(
            """SELECT latitude, longitude FROM locations WHERE location_id="""
            + str(locations[0])
        )
        data = next(iter(loc))
        lat = data[0]
        lon = data[1]
    ret_json["latitude"] = lat
    ret_json["longitude"] = lon

    return ret_json


# return a JSON object with they keys corresponding to the columns names for
# the animals table and the values corresponding to the actual data
@app.route("/api/getanimal/<animal_id>")
def get_animal(animal_id):
    animal = engine.execute(
        """SELECT * FROM animals WHERE animal_id=""" + str(animal_id)
    )
    ret_json = build_json(next(iter(animal)), animal_column_names)

    # include the latitude and longitude for one of the locations the animal
    # is in
    locations = ret_json["location_ids"]
    lat = None
    lon = None
    if locations is not None:
        loc = engine.execute(
            """SELECT latitude, longitude FROM locations WHERE location_id="""
            + str(locations[0])
        )
        data = next(iter(loc))
        lat = data[0]
        lon = data[1]
    ret_json["latitude"] = lat
    ret_json["longitude"] = lon

    return ret_json


# return a JSON object with they keys corresponding to the columns names for
# the animals table and the values corresponding to the actual data
@app.route("/api/getlocation/<location_id>")
def get_location(location_id):
    location = engine.execute(
        """SELECT * FROM locations WHERE location_id=""" + str(location_id)
    )
    return build_json(next(iter(location)), location_column_names)


# build a dictionary with the keys being the column names and the values being
# the data passed in
# because we executed the SQL command to get the column names, we will get the
# data in the order that matches the column names
def build_json(data, names):
    it = iter(data)
    ret_json = {}
    for col in names:
        ret_json[col] = next(it)

    return ret_json
