# Flask Backend for The Plant Planet
---
### How to run the app
#### MacOS and Linux
The following commands only need to be run once to set up your virtual environment.
1. `python3 -m pip install --user virtualenv`
2. `python3 -m venv venv`

The following step should be run everytime you need to activate your virtual environment.

3. `source venv/bin/activate`
4. `export FLASK_APP=application.py`
5. (only run first time) `pip3 install -r requirements.txt`

Now you should be able to run the app!

6. `flask run`

Hit ^C to when you're done running the app.
To exit the virtual environment type `deactivate`

---
#### Windows
The following commands only need to be run once to set up your virtual environment.
1. `py -m pip install --user virtualenv`
2. `py -m venv venv`

The following step should be run everytime you need to activate your virtual environment.

3. `.\venv\Scripts\activate`
4. `set FLASK_APP=application.py`
5. (only run first time) `pip install -r requirements.txt`

Now you should be able to run the app!

6. `flask run`

Hit CTRL+C when you're done running the app.
To exit the virtual environment type `deactivate`

---
Let me (Nina) know if you have any problems with this! You will need pip for this, and do everything from the main project folder.