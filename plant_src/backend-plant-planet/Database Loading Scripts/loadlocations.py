import psycopg2
import requests
import json


def loadlocations():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="SunflowerVol.6",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )
    cursor = conn.cursor()

    f = open("locations.json", "r")
    data = json.loads(f.read())

    columns = [
        "state",
        "code",
        "website",
        "capital_city",
        "capital_url",
        "population",
        "state_flag_url",
        "map_image_url",
        "landscape_background_url",
        "twitter_url",
    ]

    loc = 0
    for state in data:
        if 0 == 0:
            data = []
            for col in columns:
                val = state[col]
                if val is None:
                    val = " "
                data += [val]

            cursor.execute(
                "INSERT INTO locations (location_id) VALUES (" + str(loc) + ")"
            )
            data_it = iter(data)
            for col in columns:
                val = next(data_it)
                if col != "population":
                    val = "'" + val + "'"
                else:
                    val = str(val)
                cursor.execute(
                    "UPDATE locations SET "
                    + col
                    + " = "
                    + val
                    + "WHERE location_id = "
                    + str(loc)
                )

            conn.commit()
            loc += 1


def loadcanada():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="SunflowerVol.6",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )
    cursor = conn.cursor()

    f = open("locations.json", "r")
    data = json.loads(f.read())

    columns = [
        "state",
        "code",
        "website",
        "capital_city",
        "capital_url",
        "population",
        "state_flag_url",
        "map_image_url",
        "landscape_background_url",
        "twitter_url",
    ]

    all_data = [
        [
            "Newfoundland and Labrador",
            " ",
            "https://www.gov.nl.ca/",
            "St. John''s",
            " ",
            521542,
            "https://cdn.britannica.com/s:500x350/01/63001-004-155A6664/MOTTO-"
            + "Quaerite-Prime-Regnum-Dei.jpghttps://upload.wikimedia.org/wikip"
            + "edia/commons/thumb/d/dd/Flag_of_Newfoundland_and_Labrador.svg/1"
            + "200px-Flag_of_Newfoundland_and_Labrador.svg.png",
            " ",
            " ",
            "https://twitter.com/govnl?lang=en",
        ],
        [
            "Nova Scotia",
            " ",
            "https://novascotia.ca/government/",
            "Halifax Regional Municipality",
            " ",
            971395,
            "https://upload.wikimedia.org/wikipedia/commons/c/c0/Flag_of_Nova_"
            + "Scotia.svg",
            " ",
            " ",
            "https://twitter.com/nsgov",
        ],
        [
            "Ontario",
            " ",
            "https://www.ontario.ca/page/government-ontario",
            "Toronto",
            " ",
            14570000,
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_"
            + "Ontario.svg/255px-Flag_of_Ontario.svg.png",
            " ",
            " ",
            "https://twitter.com/ONgov",
        ],
        [
            "Prince Edward Island",
            " ",
            "https://www.princeedwardisland.ca/en",
            "Charlottetown",
            " ",
            156947,
            "https://upload.wikimedia.org/wikipedia/commons/d/d7/Flag_of_Princ"
            + "e_Edward_Island.svg",
            " ",
            " ",
            "https://twitter.com/infopei?lang=en",
        ],
        [
            "Quebec",
            " ",
            "https://www.quebec.ca/en/",
            "Quebec City",
            " ",
            542298,
            "https://upload.wikimedia.org/wikipedia/commons/5/5f/Flag_of_Quebe"
            +"c.svg",
            " ",
            " ",
            "https://twitter.com/gouvqc?lang=en",
        ],
        [
            "Saskatchewan",
            " ",
            "https://www.saskatchewan.ca/",
            "Regina",
            " ",
            1174000,
            "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Flag_of"
            + "_Saskatchewan.svg/1200px-Flag_of_Saskatchewan.svg.png",
            " ",
            " ",
            "https://twitter.com/SKGov",
        ],
    ]

    loc = 54
    for state in all_data:
        if 0 == 0:
            cursor.execute(
                "INSERT INTO locations (location_id) VALUES (" + str(loc) + ")"
            )
            data_it = iter(state)
            for col in columns:
                val = next(data_it)
                if col != "population":
                    val = "'" + val + "'"
                else:
                    val = str(val)
                cursor.execute(
                    "UPDATE locations SET "
                    + col
                    + " = "
                    + val
                    + " WHERE location_id = "
                    + str(loc)
                )

            conn.commit()
            loc += 1


def linkplants():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="SunflowerVol.6",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )
    cursor = conn.cursor()

    cursor.execute("""SELECT plant_id, state_code FROM plants""")
    plants = cursor.fetchall()
    oops = 0
    for plant in plants:
        state_codes = []
        states = plant[1]

        if states is not None:
            for state in states:
                cursor.execute(
                    """SELECT location_id, plant_ids FROM locations WHERE"""
                    + """ state = \'"""
                    + state
                    + """\'"""
                )
                try:
                    loc = cursor.fetchall()[0]
                    state_codes += [loc[0]]
                    plant_ids = loc[1]
                    if loc[1] == None:
                        plant_ids = []
                    plant_ids += [plant[0]]
                    cursor.execute(
                        """UPDATE locations SET plant_ids = ARRAY"""
                        + str(plant_ids)
                        + """ WHERE location_id = """
                        + str(loc[0])
                    )
                except:
                    oops += 1

            cursor.execute(
                """UPDATE plants SET location_ids = ARRAY"""
                + str(state_codes)
                + """ WHERE plant_id = """
                + str(plant[0])
            )

    conn.commit()


def linkanimals():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="SunflowerVol.6",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )
    cursor = conn.cursor()

    cursor.execute("""SELECT animal_id, state_code FROM animals""")
    animals = cursor.fetchall()
    oops = 0
    for animal in animals:
        state_codes = []
        states = animal[1]

        if states is not None:
            for state in states:
                cursor.execute(
                    """SELECT location_id, animal_ids FROM locations WHERE"""
                    + """ state = \'"""
                    + state
                    + """\'"""
                )
                try:
                    loc = cursor.fetchall()[0]
                    state_codes += [loc[0]]
                    animal_ids = loc[1]
                    if loc[1] == None:
                        animal_ids = []
                    animal_ids += [animal[0]]
                    cursor.execute(
                        """UPDATE locations SET animal_ids = ARRAY"""
                        + str(animal_ids)
                        + """ WHERE location_id = """
                        + str(loc[0])
                    )
                except:
                    conn.rollback()

            cursor.execute(
                """UPDATE animals SET location_ids = ARRAY"""
                + str(state_codes)
                + """ WHERE animal_id = """
                + str(animal[0])
            )
            conn.commit()


def load_lat_long():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="SunflowerVol.6",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )
    cursor = conn.cursor()

    cursor.execute("""SELECT state FROM locations""")
    states = cursor.fetchall()

    for state_tuple in states:
        state = state_tuple[0]

        response = requests.get(
            "http://api.geonames.org/searchJSON?q='"
            + state
            + "'&maxRows=10&username=varshusree"
        )
        result_json = response.json()
        code = result_json["geonames"][0]["geonameId"]

        response = requests.get(
            "http://api.geonames.org/getJSON?geonameId="
            + str(code)
            + "&username=varshusree&style=full"
        )
        result_json = response.json()
        lat = result_json["lat"]
        lng = result_json["lng"]
        country = result_json["countryName"]

        cursor.execute(
            "UPDATE locations SET latitude = "
            + str(lat)
            + ", longitude = "
            + str(lng)
            + ", country = '"
            + country
            + "' WHERE state = '"
            + state
            + "'"
        )

    conn.commit()


if __name__ == "__main__":
    load_lat_long()
