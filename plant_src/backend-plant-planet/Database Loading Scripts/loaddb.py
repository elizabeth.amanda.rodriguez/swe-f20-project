import psycopg2
from psycopg2 import OperationalError, errorcodes, errors
import requests
import random


def getPlantInfo():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="***",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )
    cursor = conn.cursor()
    cursor.execute("""SELECT plant_id, scientific_name FROM plants""")
    ids_from_db = cursor.fetchall()
    trefle_plant_ids = []
    i = 0
    for p_id, p_name in ids_from_db:
        request_str = f"https://trefle.io/api/v1/plants/search/?q='{p_name}’;"
        + "token=cwZYwrokbP5mgFN-yxIwRiWg-JlNaC1jjvuyTr_qLqQ"
        response = requests.get(request_str)
        if response.status_code == 200:
            plantjson = response.json()
        for plant in plantjson["data"]:
            print(plant["scientific_name"])
            if plant["scientific_name"] == p_name:  # Add [1] back to name
                trefle_plant_ids.append(
                    (plant["id"], p_name, p_id)
                )  # Add [1] back to name
                print("MATCH")
                break
    print(trefle_plant_ids)
    # trefle_plants_ids (trefle_plant_id, scientific_name, our_id)
    for trefle_id, p_name, p_id in trefle_plant_ids:
        request_str = f"https://trefle.io/api/v1/plants/{trefle_id}/?"
        + "token=cwZYwrokbP5mgFN-yxIwRiWg-JlNaC1jjvuyTr_qLqQ"
        # print(request_str)
        response = requests.get(request_str)
        if response.status_code == 200:
            plantjson = response.json()
            var = plantjson["data"]["image_url"]
            img_url = plantjson["data"]["image_url"]
            genus = plantjson["data"]["main_species"]["genus"]
            family = plantjson["data"]["main_species"]["family"]
            growthhabit = plantjson["data"]["main_species"]["specifications"][
                "growth_habit"
            ]
            avg_height_min = plantjson["data"]["main_species"][
                "specifications"
            ]["average_height"]["cm"]
            avg_height_max = plantjson["data"]["main_species"][
                "specifications"
            ]["maximum_height"]["cm"]
            toxicity = plantjson["data"]["main_species"]["specifications"][
                "toxicity"
            ]
            daystoharvest = plantjson["data"]["main_species"]["growth"][
                "days_to_harvest"
            ]
            ph = plantjson["data"]["main_species"]["growth"]["ph_maximum"]
            light = plantjson["data"]["main_species"]["growth"]["light"]
            humidity = plantjson["data"]["main_species"]["growth"][
                "atmospheric_humidity"
            ]
            growth_months = plantjson["data"]["main_species"]["growth"][
                "growth_months"
            ]
            root_depth = plantjson["data"]["main_species"]["growth"][
                "minimum_root_depth"
            ]["cm"]
            soil_nutrients = plantjson["data"]["main_species"]["growth"][
                "soil_nutriments"
            ]
            soil_texture = plantjson["data"]["main_species"]["growth"][
                "soil_texture"
            ]
            soil_humidity = plantjson["data"]["main_species"]["growth"][
                "soil_humidity"
            ]
            fields = [("img_url", img_url)]
            for col_name, col_val in fields:
                if col_val is not None:
                    cval = str(col_val)
                    update_str = f"UPDATE plants SET {col_name} = "
                    + ""'{{\"{cval}\"}}' WHERE plant_id = {p_id};"
                    # cursor.execute(update_str)
                    print(update_str)


def getplantvals():
    plantset = {
        143510,
        147636,
        145130,
        147321,
        158680,
        130158,
        159313,
        122342,
        132078,
        144360,
        140989,
        153299,
        135052,
        158387,
        127164,
        128384,
        157260,
        140538,
        129907,
        134277,
        126007,
        125900,
    }
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="***",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()

    while len(plantset) < 100:
        ending = random.randrange(200000)
        response = requests.get(
            "https://explorer.natureserve.org/api/data/taxonSearch?"
            + "ouSeqUid=ELEMENT_GLOBAL.2."
            + str(ending)
        )
        if response.status_code == 200 and not plantset.__contains__(ending):
            plantjson = response.json()

            if (
                plantjson["nameCategory"]["nameTypeCd"] == "P"
                and plantjson["primaryCommonName"] is not None
                and plantjson["scientificName"] is not None
            ):
                plantset.add(ending)
                common_name = plantjson["primaryCommonName"]
                scientific_name = plantjson["scientificName"]
                escaped_name = common_name.translate(
                    str.maketrans({"'": "''"})
                )

                cursor.execute(
                    """INSERT INTO plants (plant_id, common_name, """
                    + """scientific_name)"""
                    + """ VALUES ("""
                    + str(ending)
                    + """, \'"""
                    + escaped_name
                    + """\', \'"""
                    + scientific_name
                    + """\');"""
                )

    conn.commit()


def getanimalvals():
    animalset = {0}

    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="***",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()

    while len(animalset) < 101:
        ending = random.randrange(200000)
        response = requests.get(
            "https://explorer.natureserve.org/api/data/taxonSearch?"
            + "ouSeqUid=ELEMENT_GLOBAL.2."
            + str(ending)
        )
        if response.status_code == 200 and not animalset.__contains__(ending):
            animaljson = response.json()

            if (
                animaljson["nameCategory"]["nameTypeCd"] == "A"
                and animaljson["primaryCommonName"] is not None
                and animaljson["scientificName"] is not None
            ):
                animalset.add(ending)
                common_name = animaljson["primaryCommonName"]
                scientific_name = animaljson["scientificName"]
                escaped_name = common_name.translate(
                    str.maketrans({"'": "''"}))

                cursor.execute(
                    """INSERT INTO animals (animal_id, common_name,"""
                    + """ scientific_name)"""
                    + """ VALUES ("""
                    + str(ending)
                    + """, \'"""
                    + escaped_name
                    + """\', \'"""
                    + scientific_name
                    + """\');"""
                )

    conn.commit()


def loadlocations():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="***",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()
    cursor.execute("""SELECT animal_id FROM animals""")
    ids = cursor.fetchall()

    for end in ids:
        ending = end[0]
        response = requests.get(
            "https://explorer.natureserve.org/api/data/taxonSearch"
            +" ?ouSeqUid=ELEMENT_GLOBAL.2."
            + str(ending)
        )
        plantjson = response.json()
        locations = plantjson["elementNationals"]
        countries = []

        for i in range(len(locations)):
            iso = plantjson["elementNationals"][i]["nation"]["isoCode"]
            countries += [str(iso)]

        cursor.execute(
            """UPDATE animals SET country_code = ARRAY"""
            + str(countries)
            + """ WHERE animal_id = """
            + str(ending)
        )
        conn.commit()


def loadstates():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="***",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()
    cursor.execute("""SELECT plant_id FROM plants""")
    ids = cursor.fetchall()

    for end in ids:
        ending = end[0]
        response = requests.get(
            "https://explorer.natureserve.org/api/data/"
            + "taxonSearch?ouSeqUid=ELEMENT_GLOBAL.2."
            + str(ending)
        )
        plantjson = response.json()
        locations = plantjson["elementNationals"]

        for i in range(len(locations)):
            states = plantjson["elementNationals"][i]["elementSubnationals"]
            names = []

            for j in range(len(states)):
                iso = plantjson["elementNationals"][i]["elementSubnationals"][
                    j
                ]["subnation"]["nameEn"]
                names += [iso]

            if len(names) > 0:
                cursor.execute(
                    """UPDATE plants SET state_code = ARRAY"""
                    + str(names)
                    + """ WHERE plant_id = """
                    + str(ending)
                )
                conn.commit()


def loadTaxonomy():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="****",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()
    cursor.execute("""SELECT plant_id FROM plants""")
    ids = cursor.fetchall()

    for end in ids:
        ending = end[0]
        response = requests.get(
            "https://explorer.natureserve.org/api/data/"
            + "taxonSearch?ouSeqUid=ELEMENT_GLOBAL.2."
            + str(ending)
        )
        animaljson = response.json()

        family = animaljson["speciesGlobal"]["family"]
        genus = animaljson["speciesGlobal"]["genus"]

        cursor.execute(
            """UPDATE plants SET family = \'"""
            + str(family)
            + """\', genus = \'"""
            + str(genus)
            + """\' WHERE plant_id = """
            + str(ending)
        )

        conn.commit()


def loadInformalTax():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="****",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()
    cursor.execute("""SELECT animal_id FROM animals""")
    ids = cursor.fetchall()

    for end in ids:
        ending = end[0]

        response = requests.get(
            "https://explorer.natureserve.org/api/data/"
            + "taxonSearch?ouSeqUid=ELEMENT_GLOBAL.2."
            + str(ending)
        )
        animaljson = response.json()

        tax = animaljson["rankInfo"]["popSize"]

        if tax != None:
            tax = animaljson["rankInfo"]["popSize"]["popSizeDescEn"]
        else:
            tax = "N/A"

        cursor.execute(
            """UPDATE animals SET population = \'"""
            + str(tax)
            + """\' WHERE animal_id = """
            + str(ending)
        )

        conn.commit()


def loadAnimalImages():
    conn = psycopg2.connect(
        database="postgres",
        user="nina",
        password="SunflowerVol.6",
        host="planttest.c8reqlopldwe.us-east-1.rds.amazonaws.com",
        port=5432,
    )

    cursor = conn.cursor()
    cursor.execute("""SELECT plant_id FROM plants""")
    ids = cursor.fetchall()
    count = 0
    for end in ids:
        print(count)
        # if count == 13: #98
        #     count += 1
        #     continue
        count += 1

        ending = end[0]

        try:
            cursor.execute(
                """SELECT img_url FROM plants WHERE plant_id = """ + str(ending)
            )
            img = cursor.fetchone()[0]
            if img != None:
                continue

            cursor.execute(
                """SELECT common_name FROM plants WHERE plant_id = """
                + str(ending)
            )
            name = cursor.fetchone()[0]

            response = requests.get(
                "https://customsearch.googleapis.com/customsearch/v1?cx="
                + "7c538e28d55769e1d&key=AIzaSyCBeSULjtzmnncQ4WCygaCVfahW"
                + "Dcdn28k&num=10&filetype='jpg'&q='"
                + str(name)
                + "'"
            )

            animaljson = response.json()

            images = []
            for i in range(len(animaljson["items"])):
                try:
                    print("Here!")
                    images += [
                        str(
                            animaljson["items"][i]["pagemap"]["cse_image"][0][
                                "src"
                            ]
                        )
                    ]
                    print("Added image!")
                except:
                    pass
            print("All images")
            print(images)
            if len(images) > 0:

                cursor.execute(
                    """UPDATE plants SET img_url = ARRAY"""
                    + str(images)
                    + """ WHERE plant_id = """
                    + str(ending)
                )

                conn.commit()

        except:
            conn.rollback()


if __name__ == "__main__":
    loadAnimalImages()
