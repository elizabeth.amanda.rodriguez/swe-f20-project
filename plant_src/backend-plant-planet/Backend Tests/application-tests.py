# Possible tests:
# check if NULL for all?

# internal server error, get_plant/animal/location without valid id

from flask import Flask
from unittest import main, TestCase
import sys
sys.path.append('./..')

import application

###########################
# ApplicationsTests class #
###########################

class ApplicationTests (TestCase) :

    ###############
    # model tests #
    ###############

    def test1_get_plant_model (self) :
        app = Flask(__name__)
        with app.app_context() :
            self.assertIsNotNone(application.get_plant_model())

    def test1_get_animal_model (self) :
        app = Flask(__name__)
        with app.app_context() :
            self.assertIsNotNone (application.get_animal_model())

    def test1_get_location_model (self) :
        app = Flask(__name__)
        with app.app_context() :
            self.assertIsNotNone (application.get_location_model())

    ###################
    # get_plant tests #
    ###################


    def test1_get_plant (self) :
        plant_id = 146969
        self.assertEqual (application.get_plant(plant_id)["family"], "Fabaceae")

    def test2_get_plant (self) :
        plant_id = 146969
        self.assertEqual (application.get_plant(plant_id)["common_name"], "Syrian Mesquite")
    
    def test3_get_plant (self) :
        plant_id = 146969
        self.assertEqual (application.get_plant(plant_id)["plant_id"], 146969)

    def test4_get_plant (self) :
        plant_id = 145962
        self.assertIsNotNone (application.get_plant(plant_id))

    def test5_get_plant (self) :
        plant_id = 156032
        self.assertIsNotNone (application.get_plant(plant_id))
    
    def test6_get_plant (self) :
        plant_id = 158562
        self.assertIsNotNone (application.get_plant(plant_id))

    ####################
    # get_animal tests #
    ####################

    def test1_get_animal (self) :
        animal_id = 120677
        self.assertEqual (application.get_animal(animal_id)["common_name"], "Broadshoulder Physa")

    def test2_get_animal (self) :
        animal_id = 100828
        self.assertEqual (application.get_animal(animal_id)["phylum"], "Craniata")
    
    def test3_get_animal (self) :
        animal_id = 113148
        self.assertEqual (application.get_animal(animal_id)["animal_order"], "Coleoptera")

    def test4_get_animal (self) :
        animal_id = 121124
        self.assertIsNotNone (application.get_animal(animal_id))

    def test5_get_animal (self) :
        animal_id = 120808
        self.assertIsNotNone (application.get_animal(animal_id))
    
    def test6_get_animal (self) :
        animal_id = 112584
        self.assertIsNotNone (application.get_animal(animal_id))

    ######################
    # get_location tests #
    ######################

    def test1_get_location (self) :
        location_id = 30
        self.assertEqual (application.get_location(location_id)["state"], "New Mexico")

    def test2_get_location (self) :
        location_id = 4
        self.assertEqual (application.get_location(location_id)["code"], "CA")
    
    def test3_get_location (self) :
        location_id = 15
        self.assertEqual (application.get_location(location_id)["website"], "https://www.kansas.gov")

    def test4_get_location (self) :
        location_id = 13
        self.assertIsNotNone (application.get_location(location_id))

    def test5_get_location (self) :
        location_id = 44
        self.assertIsNotNone (application.get_location(location_id))
    
    def test6_get_location (self) :
        location_id = 2
        self.assertIsNotNone (application.get_location(location_id))

########
# main #
########

def create_app():
    app = Flask(__name__)

    with app.app_context():
        init_db()

    return app

if __name__ == "__main__":  # pragma: no cover
    main()