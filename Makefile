all:

flaskapp:
	( \
		cd plant_src/plant-planet; \
		yarn build; \
		cd ../backend-plant-planet; \
		source venv/bin/activate; \
		export FLASK_APP=application.py; \
		flask run; \
	)